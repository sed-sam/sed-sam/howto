[Retour à l'index](./README.md)


# how to build artifacts with gitlab-ci and publish it on repo-sam.inria.fr

## objective

Publish automatically artifacts created by gitlab-runners on our conda channel on repo-sam.inria.fr

## context/prerequesite

- use one (or several) gitlab-runners to create conda packages
- use a (new) gitlab-runner on srv-dream.inria.fr to pull the newly created package(s)
- trigger the conda indexes rebuilt after downloading the new package(s)

Remark:
- the actual solution is designed for conda packages but can be extended to any artifacts (.dmg, .zip, .msi, .tar.gz,...)

## Solution

The solution uses:
- the artifacts macros available for gitlab-ci
- the https://gitlab.inria.fr/sed-sam/sed-sam/conda-tools scripts (which is deployed on srv-dream.inria.fr)

## assumptions

Using this solution, the artifacts created by the `build` runners must be contained in the build tree of the builded package.

This implies that the conda package created by the `conda-build/mamba-build` process on the build runners **must** be moved after their creation.

In order to simplify the installation script (from conda-tools), the conda packages **must** be put (after its creation) in a directory which respect the conda naming convention, aka:

  - noarch
  - linux-32 / linux-64
  - osx-64 / osx-arm64
  - win-32 / win-64

The tag of the deployement runner is `deploy`


## gitlab runners (build and deploy)

We assume that you already dispose of as many as many gitlab-runners as artifacts that you want to build (maxOSX, linux, etc..)

We also suppose that a gitlab-runner instance (responsible of deploying the packages) is already installed on srv-dream.inria.fr

As a reminder, this is how the gitlab-runner service has been declared:

```
$ ssh srv-dream
$ sudo -i
# curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
# chmod +x /usr/local/bin/gitlab-runner
# /usr/local/bin/gitlab-runner install --user=jls --working-directory=/home/conda/artifacts
# /usr/local/bin/gitlab-runner start
# /usr/local/bin/gitlab-runner status
```


## configuration of your gitlab repository

Also as a reminder, you **must** activate the CI/CD of your gitlab repository:

- from the left menu of your repository main page, follow the tree:

  - Settings - General - Visibility - CI/CD - activate it

- you can now find the token associated with your repository, following the tree:

  - Settings - CI/CD - Runners

  - you will find the following information:

```
Install GitLab Runner and ensure it's running.
Register the runner with this URL:
https://gitlab.inria.fr/

And this registration token:
GR1348941YYXcpeoye8wtou8sHXsr
```

This token will be used each time you declare a new runner.


## add a gitlab-runner on srv-dream for your project

You **must** declare your token/project on srv-dream.inria.fr


  - connect to srv-dream.inria.fr with ssh: `ssh srv-dream.inria.fr`
  - become root: `sudo -i`  (remark: is it necessary ?)
  - register as a runner with the information:

    - GitLab instance URL: https://gitlab.inria.fr/
    - Token: YOUR.SECRET.TOKEN
    - Description: Package deployement (or whatever)
    - Tag: deploy
    - Executor: shell


```
# gitlab-runner register
Runtime platform                                    arch=amd64 os=linux pid=450264 revision=32fc1585 version=15.2.1
Running in system-mode.

Enter the GitLab instance URL (for example, https://gitlab.com/):
https://gitlab.inria.fr/
Enter the registration token:
GR1348941YYXcpeoye8wtou8sHXsr
Enter a description for the runner:
[ci]: Package deployement
Enter tags for the runner (comma-separated):
deploy
Enter optional maintenance note for the runner:

Registering runner... succeeded                     runner=GR1348941YYXcpeoy
Enter an executor: custom, parallels, ssh, virtualbox, docker+machine, docker, docker-ssh, shell, docker-ssh+machine, kubernetes:
    shell
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!

Configuration (with the authentication token) was saved in "/etc/gitlab-runner/config.toml"
```

- check that this runner is known on the gitlab project page (https://gitlab.inria.fr/jls/sandbox/-/settings/ci_cd, click on Runners )


## gitlab-ci.yml configuration file


### content

This is an example of a gitlab-ci file which uses the artifacts macros and the conda-tools on srv-dream.inria.frci

```
build-linux-develop:
  stage: build
  tags:
      - linux
  only:
      -  develop
  script:
      - source /home/gitlab-runner/.bashrc
      - mamba build purge-all
      - mamba build pkg/recipe --no-anaconda-upload -c https://repo-sam.inria.fr/sedsam/conda/m4h -c conda-forge
      - mkdir linux-64
      - cp /home/gitlab-runner/mambaforge/conda-bld/linux-64/*.tar.bz2 ./linux-64/
  artifacts:
    paths:
      - linux-64/
  variables:
      GIT_STRATEGY: clone

deploy:
  stage: deploy
  tags:
      - deploy
  only:
      - develop
  script:
      - /home/conda/conda-tools/bin/deploy-conda -d m4h
```

- run the pipepline and verify the artifacts on srv-dream.inria.fr
```
$ pwd
/home/conda/artifacts/builds/QJRomLyp/0/demagus/m4h-acquisition-server

$ tree -L 1
./
├── CMakeLists.txt
├── data/
├── linux-64/
├── pkg/
├── README.md
├── src/
└── tst/

6 directories, 2 files

$ tree linux-64/
linux-64//
└── m4h-acquisition-server-0.0.1-h2bc3f7f_0.tar.bz2

1 directory, 1 file
```


### explanation

Compared to a usual gitlab-ci file, there are only few changes:

- the use or `artifacts` for the build runners
- the obligation to put the created conda packages into a dedicated directory (here: `linux-64`)
- the use of `/home/conda/conda-tools/bin/deploy-conda` in the deploy section


### conda-tools usage

```
$ /home/conda/conda-tools/bin/deploy-conda -h
usage: /home/conda/conda-tools/bin/deploy-conda -d dir

deploy gitlab artifacts on repo-sam after ci build

-d|--dest dir  final destination of the package.
               dir can be absolute or a subdir of
               /u/team-sam/dream_norep/sedsam/conda
               (MANDATORY)
-h|--help      this help
-n|--dry_run   print what should be done instead of doing it
```

### remarks

In case of multiple build phases:

- all the build runners are executed in parralel.
- the deploy phase is runner after all build runners have completed and will receive all artifacts
- as so, a single `deploy` section is declared
- all artifacts are also available directly on gitlab.inria.fr, in the build job resume (e.g.: https://gitlab.inria.fr/demagus/m4h-acquisition-server/-/jobs/3267276)
