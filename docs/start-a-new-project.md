[go back to index](./README.md)

# How to start a new project

## introduction

this is a checklist of all tools to install then starting a new A(M)DT project. This guide
provides also tools and tips on how to configure these tools

in this document, the ${ADTNAME} should be replaced by.... the adt name (e.g. sw2d, precis...)

## communication

### mailists

Declaration of the two following mailistes on the **sympa.inria.fr** server

* amdt-${ADTNAME}@inria.fr: contains amdt-team@inria.fr (as data input/"source de données"). Contains also all members of the research team.
* amdt-${ADTNAME}-dev@inria.fr: contains the previous list (amdt-${ADTNAME}@inria.fr) as data input/"source de données".

The first list is used for human to human communication. The "-dev" list is used then configuring the development tools (gitlab, ci, etc..)

Both list are declared as "public, not opened and no spam" lists:

* visible de l'intranet
* liste ouverte, Bcc interdit
* visible de l'intranet
* abonnement ouvert à tous après autorisation du propriétaire
* désabonnement après confirmation par mail, notification du propriétaire
* invitation à l'abonnement fermée
* list des abonnés accessibles à abonnés et domaine local
* pas d'archives
* propriétaires: add all admt-team members (by hand, no way to add a list in this interface)


### discord

The realtime communication during the sprint use discord (https://discordapp.com/)
Every body should have a discord account. Every time a new channel is created, an invitation link should be posted to the amdt-${ADTNAME}@inria.fr list.

We use the follonwing discord organisation:

* you should create a new server named: ${ADTNAME}. If the name is already taken, use your imagination !
* you should also create the following channels:

    * **server/#welcome**: presentation of the discord channels and rules
    * **server/#activity**: all activity logs for gitlab, ci, bots, etc..
    * **server/#announcements**: server-wide announcements are posted here; mostly to do with dtk's development (release cycles, major bug fixes, breaking changes, hiatuses, etc) or changes to the Discord server.

    * **chat/#lounge**: a place where everybody knows your name. Talk about whatever you like. Keep it clean.
    * **chat/#help**: for users to report bugs, ask for assistance, or troubleshoot dtk.
    * **chat/#development**: a place to discuss development in general or its source code. The right place to discuss code design decisions, development plans or the general management of the project.
* invite members from the ADT to the server
* create an \@admin role, add all general/text/voice permissions (except "priority speaker")
* add \@admin role to amdt-team members 

All server channels are readonly (only maintainer and bots are allowed to post).


## gitlab server

### main configuration

In order to collect all codes, a gitlab group (named ${ADTNAME}) should be created.

All AMDT team members and all research team members should be added as members of this group with a (at least) developper account.

Initialize gitflow, make develop the default branch

### repositories

The main code repository should also be named ${ADTNAME}. Consequently, the cloning URL of the repository is: **git@gitlab.inria.fr:${ADTNAME}/${ADTNAME}.git**

The main repository should not contains data, except for the non-regression tests (aka: small bunches of test data). If there is a need to share data for benchmark or demonstration/exhibition purpose a separate repository should be created (e.g.:  ${ADTNAME}-data).


### using the gitlab's issues

We use the **issues** tab to follow the user stories of the development cycles. The scrum master is in charge of the life cycle changes of all issues.

#### labels

You should declare **at least** the following labels:

* user stories labels:

    * labels **#1** .. **#9** will be used to enumerate the user stories
    * each task associated with a given user story should use the same label

* task status: each task should be assigned a status, showing its completion status

    * To Do (soft orange: #F0AD4E): the task is programmed but nobody works on it
    * Doing (greenL #69D100): someone is working on the given task
    * To Merge (dark saturated blue #34495E): a merge request as been issued, someone may complete the merge
    * Done (pure red #FF0000): the task is completed
    * Postponed (ivery pale orange #FFECDB): the task is delayed

* other labels: the team may also use additional labels. For example, one may use a description of a code modification (bug, enhancement, performance...)


## ci server configuration

Connect to ci.inria.fr and create a ${ADTNAME} project as a private project.

Create as many slaves as necessary, usually one per architecture/OS, e.g. linux, macosX, MSWindows
We usually use the names :

* linux: ${ADTNAME}-fedora-NUMBER (ex: macular-fedora-26)
* mac: ${ADTNAME}-macosx
* windows: ${ADTNAME}-windows-NUMBER (ex: macular-windows-7)

If you use the inextremis host to compile under macOSX, use the name **inextremis-zie**

Jobs on each slave should be also contain the name os the architecture and the project name or plugin or...

## inter tools communication

### gitlab/discord

Goto project settings, integration submenu and validate the **Discord Notifications** in the **Project Services** section.

The webhook to declare should be found on Discord, in the **#activity** configuration pane (Edit Channel), **Webhook** tab, **Create Webhook** button.
(you must have admin privilege on Discord server in order to achieve it).

### gitlab/ci

https://iww.inria.fr/sed-sophia/gitlab-inria-frci-inria-fr-integration/

## coding environment and conventions

### coding style

https://gitlab.inria.fr/dtk/dtk-core/-/wikis/Coding-Style

### gitflow paradigm

We use the gitflow paradigm described at https://danielkummer.github.io/git-flow-cheatsheet/

### description files

The topdir directory should contain:

* **README.md**: describes the project, requirements, howto compile the project
* **CHANGES.md**: list all changes between versions

### licence

**TODO**

### release policy and process

* use gitflow to do a release
* update the build numbers in **pkg/recipe/meta.yaml** files
* update the version neumber in topdir **CMakeLists.txt** file
* update the **CHANGES.md** file
* change and publish git tags

### code tree organisation

This is a typical directory tree:

```
.
├── app
│   └── ${ADTNAME}TemplateEngine
├── build
├── cmake
├── doc
├── pkg
│   ├── env
│   └── recipe
├── src
│   ├── ${ADTNAME}Core
│   └── ${ADTNAME}Widgets
└── tst
    ├── ${ADTNAME}Core
    └── resources
```

* app: contains all program/binary source
* build: build dir (no kidding !)
* cmake: contains the .cmake files associated with the project (if applicable)
* doc: doxygen configuration file and other documentation input
* pkg: contains both the conda build environment (recipe) and the developement environment (pkg)
* src: contains the source code of the libraries. Libraries should be separated
* tst: all test code. The **resource** subdirectory contains the test data (may contain binary files)*

### conda environment

Each project is compiled in its own **conda** environment. As described above, a conda.env file is provided to build a local conda development environment.
A typical way to proceed is (the README.md file may be more precise):

```
$ git clone git@gitlab.inria.fr:${ADTNAME}/${ADTNAME}.git**
$ cd ${ADTNAME}
$ git flow init
...
$ conda env update -f ./pkg/env/${ADTNAME}.env
$ conda activate ${ADTNAME}
$ mkdir build
$ cd build
$ cmake -DCMAKE_PREFIX_PATH:PATH=${CONDA_PREFIX} -DCMAKE_INSTALL_PATH:PATH=${CONDA_PREFIX} ..
```

### cmake

**TODO**


## documentation

We usually use **doxygen** to create the developper documentation.

If you want to refer to external doxygen documentation (e.g. for Qt, dtk-*, VTK), you may use the external tagging mechanism of doxygen.

Readm https://iww.inria.fr/sed-sophia/create-a-gitlab-page-for-a-given-project/ for more information.

## end of sprint demonstration

### invitations for the demo

Use (and update if needed) the amdt-demo@inria.fr to invite people to the demo.

### start the visualisation system

**TODO**

###  Immersive space usage

The **git@gitlab.inria.fr:sed-sam/sed-sam/salle_immersive.git** describe the usage of the immersive room.

In particular, the file organisation of the linux host is: https://gitlab.inria.fr/sed-sam/sed-sam/salle_immersive/blob/master/demos/organisation.md


### starting a visio

* launch the Chromium browser (right click on desktop, section **Application**, subsection **Internet**, application **Chromium browser**)
* click on the first memorized link (**START A VISIO**), which should launch: [****see corresponding file in our private gitlab repo.**](https://gitlab.inria.fr/sed-sam/sed-sam/howto/-/tree/master/docs-private/start-a-new-project.md)
* select a camera and a microphone, both of each should work

   * one camera is facing the screen
   * the other camera faces the public

* join the meeting

#### visio scenario for a demo

From PC-SYS (Windows) :
* launch the Chrome browser
* join the visio room selecting a camera and a microphone (they are connected this machine)
* turn off sound input, if using a mobile phone for presenter sound
* turn off sound output for this machine

From PC-INRIA (Linux) :
* launch the Chromium browser
* join the visio room with no camera, no microphone
* share your screen or window in the visio
* record the demo (see below)
* tune the sound output level for this machine (immersive room speakers are connected to the active machine in the room, so they switched automatically to this machine)

From the immersive room control panel :
* mute the room speakers during the presentation

During the presentation : 
* if using a mobile phone for presenter sound, join visio with the mobile phone
  * either by phone - Phone: [****see corresponding file in our private gitlab repo.**](https://gitlab.inria.fr/sed-sam/sed-sam/howto/-/tree/master/docs-private/start-a-new-project.md)
  * either with visio web client on a smartphone
* for visio users questions at the end of the presentation
  * unmute the room speakers only if using smartphone for visio users question (too much echo with room micro)
  * request visio users question by chat

A laptop could replace PC-INRIA.

### video recording

* linux: **simplescreenrecorder** is a simple for recording both the video and the audio.

* macOS:

  * vido capture: https://obsproject.com/fr/download
  * subtitle edition: http://www.aegisub.org/
  * video editing: http://avidemux.sourceforge.net/ or  https://www.openshot.org/fr/download/
  * cursor highlight: https://download.cnet.com/Highlighter/3000-2193_4-177023.html



## useful links

* git / gitflow

  * https://danielkummer.github.io/git-flow-cheatsheet/
  * https://www.atlassian.com/git/tutorials/making-a-pull-request
  * https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow

* conda

  * https://docs.conda.io/projects/conda/en/latest/user-guide/getting-started.html
  * https://docs.conda.io/en/latest/miniconda.html
  * https://geohackweek.github.io/datasharing/01-conda-tutorial/

* coding-style

  * https://gitlab.inria.fr/dtk/dtk-core/-/wikis/Coding-Style

* inria tools:

  * https://iww.inria.fr/sed-sophia/gitlab-inria-frci-inria-fr-integration/
  * https://iww.inria.fr/sed-sophia/create-a-gitlab-page-for-a-given-project/

* cmake

  * https://cmake.org/cmake/help/latest/guide/tutorial/index.html

* agile methods :

  * https://iww.inria.fr/sed-sophia/fr/retour-dexperience-methode-agile-simplifiee/

* user stories :

  * https://www.atlassian.com/fr/agile/project-management/user-stories
  * https://blog.myagilepartner.fr/index.php/2017/08/02/user-stories-bien-les-rediger/

