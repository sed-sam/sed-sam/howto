module.exports = {
    title: 'SED Sophia - Tutoriels et mémos',
    description: 'Les tutoriels et mémos du SED.',
    dest: './public',
    base: "/sed-sam/howto/",
    head: [
        ['link', { rel: 'icon', href: '/favicon.png' }]
    ],
    theme: 'yuu',
    themeConfig: {
        nav: [
            { text: 'Accueil', link: '/' },
        ],
        sidebar: 'auto',
        search: true,
        yuu:  {
            defaultDarkTheme: false,
            defaultColorTheme: 'green',
            colorThemes: ['purple', 'blue', 'red'],
            disableThemeIgnore: true,
            extraOptions: { // add some Vue components if you want!
                // after: 'ToggleDarkMode',
            },
        }
    },
    plugins: [
        '@vuepress/back-to-top',
        '@vuepress/active-header-links'
    ],
    markdown: {
        anchor: { permalink: false, permalinkBefore: false, permalinkSymbol: '#' },
        linkify: true
    }
}
