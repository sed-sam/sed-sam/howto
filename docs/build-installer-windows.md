[Retour à l'index](./README.md)

- Ce tuto vous est présenté par Jonathan
- Je vous présente comment **utiliser conda-bundle** pour générer un installateur Windows. Pour [continuer à le développer, suivez cet article.](./develop-conda-bundle)

## 1. Préambule

Au SED, on avait régulièrement besoin de faire une release directement utilisable d'un logiciel qu'on développe. Et souvent, c'était pour des utilisateurs Windows.

Du coup, on voulait faire un installateur typique de ce qu'on trouve dans Windows, avec un écran de démarrage, choisir où on installe, etc. Le problème, c'est que comme on utilise intensivement **conda**, c'est très casse-pieds pour distribuer un environnement conda intégral sans venir affecter le reste de la machine.

Chez Anaconda.org ils ont un outil qui s'appelle **conda-constructor** pour distribuer des installateurs d'environnements conda, mais ils servent à installer une vraie distribution **conda** dans l'OS avec toutes les commandes associées et tout. C'est plus à destination des développeurs. Je suis reparti de leur **conda-constructor** et je l'ai bidouillé pour rajouter quelques propriétés intéressantes. Mon fork s'appelle **conda-bundle** et il est pour le moment encore hébergé sur [le GitHub du SED Sophia](https://github.com/SED-Inria-Sophia/conda-bundle).

Les propriétés que j'ai rajoutées :
- L'installateur généré ne vient pas interférer avec une autre installation de **conda**
- On peut rajouter des raccourcis (qui seront créés sur le bureau et le menu démarrer) à spécifier dans le fichier YAML.
- On peut enregistrer un shell pour permettre d'ouvrir les outils en ligne de commande de votre projet depuis n'importe quel dossier, avec un clic droit.
- On peut rajouter une URL à afficher à la fin de l'installateur

Pour le moment, seul Windows est à peu près supporté. J'avais tenté de faire pareil pour Linux et macOS mais je n'y ai pas passé assez de temps pour que ça soit concluant.

## 2. Installation

**conda-bundle** est construit régulièrement pas la CI du SED et peut être installé dans un environnement conda :

```bash
conda install -c conda-forge -c dtk-forge conda-bundle
```

On peut alors construire un installeur en spécifiant le path vers un fichier **construct.yaml**. Si le fichier est dans `pkg\construct\construct.yaml`, on fait alors :

```
conda bundle pkg\construct
```

**Note : pour construire un installateur Windows il est impératif d'utiliser Windows, évidemment !**

## 3. Principe de fonctionnement

**conda-bundle**, comme conda-constructeur, fonctionne avec un fichier descriptif **construct.yaml**.

Pour résumer, avec Windows, il fonctionne ainsi :
- Il lance un script python pour lire le fichier **construct.yaml**
- Le script python télécharge les paquets listés et toutes les dépendances associées
- Le script python va lire un fichier de script NSIS "template" et vient compléter les endroits du template pour coller aux données lues dans **construct.yaml**
- Puis il exécute NSIS avec le script NSIS ainsi modifié.

Pour macOS et Linux si vous générez un exécutable shell `.sh`:
- Il lance un script python pour lire le fichier **construct.yaml**
- Le script python télécharge les paquets listés et toutes les dépendances associées
- Le script python va lire un fichier de script Shell "template" et vient compléter les endroits du template pour coller aux données lues dans **construct.yaml**
- Puis il vient écrire un fichier de script Shell dans lequel il va mettre:
    * en tête de fichier le script template qu'il a complété,
    * puis il va accoler tous ses fichiers binaires à la suite
    * et dans le script shell il y a des instructions pour qu'à l'exécution il vienne trouver les fichiers binaires dans le fichier en décompressant le fichier .sh généré ; en gros il connait les offsets en octets où commencent et finissent les binaires pour les extraire.

J'avais également tenté un mode archive portable `.tar.bz2` à l'aide d'une version patchée de **conda-pack** (un autre paquet qui permet de "portabiliser" un environnement conda) mais c'était une très mauvaise idée, et ça posait beaucoup plus de problèmes que ça n'apportait de solutions. J'aurais dû l'enlever du code.

NSIS, c'est un exécutable Windows qui sert à créer des installateurs typiques. C'est ce qu'utilise à l'origine **conda-constructor** pour créer les installateurs d'Anaconda et Miniconda. Il est installé dans l'environnement conda en tant que dépendance quand vous installez **conda-bundle**.

NSIS a [une documentation très fournie sur leur site](https://nsis.sourceforge.io/Docs/), mais attention : **c'est très, très roots.**. Dans son fonctionnement, on spécifie des fonctions et des commandes qui stockent leur résultat quasi systématiquement dans des *registres* (des variables temporaires numérotées, $0 à $9 et $R0 à $R9). Si vous voulez continuer à le développer, vous devrez sûrement fouiller dedans...

## Ecriture d'un fichier **construct.yaml**

Le meilleur exemple est sûrement le fichier de SW2D. 

```yaml
name: sw2d
version: 0.9.0

install_in_dependency_order: True
ignore_duplicate_files: True
license_file: ..\..\LICENSE.md [win]
#license_file: ../../LICENSE.md [unix]
company: Inria

welcome_image: ..\..\distrib\icons\welcome_image.png [win]

channels:
  - http://conda.anaconda.org/dtk-forge/
  - http://conda.anaconda.org/conda-forge/
  - http://repo.anaconda.com/pkgs/main/

specs:
  - python=3.7
  - conda
  - sw2d=0.9.0

# Note: numbers 0, 1, ... have no meaning whatsoever. It's just to have multiple entries.
# entries "name" and "path" are mandatory, "options" and "icon" are facultative.
shortcuts:
  1:
    name: "sw2dModeler"
    path: "__INSTALL_PATH__\\Library\\bin\\sw2dModeler.exe"
    options: ""
    icon: "__INSTALL_PATH__\\icons\\sw2dModeler.ico" # You can put something like this: "__INSTALL_PATH__\\Menu\\app.ico"
  0:
    name: "sw2d Prompt"
    path: "$WINDIR\\System32\\cmd.exe"
    options: "$\\\"/K$\\\" __INSTALL_PATH__\\Scripts\\activate.bat __INSTALL_PATH__"
    icon: "__INSTALL_PATH__\\icons\\sw2d-prompt.ico"

finish_link:
  url: "https://lemon.gitlabpages.inria.fr/sw2d"
  text: "Open SW2D website"

win_register_shell: True
```

Toutes les clés entre **name** et **specs** sont des clés usuelles de **conda-constructor**. Il n'y à qu'à spécifier :
- **name** : le nom de votre programme
- **version** : le numéro de version
- **install_in_dependency_order** : comme son nom l'indique 
- **ignore_duplicate_files** : comme son nom l'indique
- **license_file** : le chemin **RELATIF AU FICHIER CONSTRUCT.YAML** de la licence
- **company** : le nom de votre entreprise/organisme
- **welcome_image** : le chemin **RELATIF AU FICHIER CONSTRUCT.YAML** de l'image affichée quand on lance l'installateur. **Format recommandé: 164px x 314px**
- **channels** : la liste des channels où chercher les paquets pour créer l'installateur
- **specs**: la liste des paquets à installer. <span style="color:red">Vous devez spécifier `conda` et `python=3.7`. Lorsqu'un paquet conda est généré, il a un tag relatif à la version de Python utilisée pour le générer (même s'il n'a rien à voir avec Python, c'est comme ça que conda fonctionne). Toutes les dépendances au SED sont compilées avec le tag relatif à Python 3.7. Ca permet à conda de trouver les bons paquets normalement.

Maintenant attardons-nous sur les nouvelles clés spécifiques à Windows.
- **shortcuts** permet de rajouter des raccourcis à créer lors de l'installation. 
  * Vous pouvez créer autant de raccourcis que vous voulez. 
  * Chaque raccourci doit être sous un objet YAML spéficique. Leur nom n'a pas d'importance : dans l'exemple ci-dessus ils sont appelés **0** et **1** mais ils auraient pu s'appeler **le_super_raccourci_de_la_gui** et **le_splendide_raccourci_du_shell**.
  * Les chemins sont **RELATIFS À L'ENDROIT OÙ EST INSTALLE VOTRE PROGRAMME**. Pour cela, il y a le mot-clé `__INSTALL_PATH__` qui sera lu et repris par le script python de conda-bundle.
  * Ca veut aussi dire que si vous voulez leur afficher une icone particulière, il faut que vos icones soient distribuées *dans le paquet conda de votre release Windows*.
  * Dans le 2e raccourci, vous voyez des symboles **$** et beaucoup de **\\**. C'est un peu complexe : 
    + les symboles **$** sont *in fine* lus par NSIS. C'est un symbole spécial pour NSIS qui permet d'échapper des caractères ou de créer des variables (par exemple **$WINDIR**).
    + Dans la ligne d'arguments de la ligne de commande, vous voyez `$\\\"/K$\\\"`. Après lecture par Python, ça deviendra `$\"/K$\"` (`\\` s'échappe en `\` et `\"` s'échappe en `"`.) PUIS après lecture par NSIS, le caractère `$\"` sera échappé en `"`, donc ça deviendra `"/K"` (c'est la façon qu'a la commande Windows pour exécuter un script au lancement).
    + C'est terriblement moche, oui, et si j'avais eu le temps pour améliorer la chose, j'aurais fait en sorte que le Python puisse parser `"/K"` dans le YAML directement (en entourant la ligne de simple quotes `'comme ça'`) puis rajoute les caractères d'échappements (`$\`) propres à NSIS de lui-même. Si vous avez envie de faire ça, ça serait sympa.
  * L'option que vous voyez sert à appeler le script interne d'initialisation de l'environnement conda distribué avec votre programme. Somme toute, vous créez un raccourci duquel tous les programmes en ligne de commande que vous distribuez sont accessibles.
- **finish_url** affiche une petite URL discrète à la fin de l'installateur (celle que personne ne lit)
- **win_register_shell** permet de rajouter un raccourci quand on fait un clic droit, pour ouvrir une commande Windows dans le dossier de son choix avec l'environnement conda interne déjà initialisé. Très pratique pour éviter de `cd` dans tous les sens !

## 4. Mots-clés pour `construct.yaml`

La documentation se trouve sur [le Github de conda-bundle, branche develop, fichier CONSTRUCT.md.](https://github.com/SED-Inria-Sophia/conda-bundle/blob/windows/CONSTRUCT.md#new-keys). J'ai rajouté quelques clés à la fin qui sont spécifiques à conda-bundle (les autres clés viennent de conda-constructor).