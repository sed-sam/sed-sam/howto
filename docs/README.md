# Sommaire
[[_TOC_]]

Bienvenue dans les tutos du SED ! On rassemble ici les tutos ou les mémos qui peuvent servir à un peu tout le monde, les développeurs du SED, les chercheurs, les externes qui débarquent et qui ont du mal à mettre le pied à l'étrier avec les pratiques de développement qu'on a chez nous.

# Tutoriels pour utiliser les outils de développement usuels

Si vous ne savez pas quoi faire, ce sont ces **tutos explicatifs pour les chercheurs** dont vous avez sûrement besoin ! Ils sont faits pour être accessibles et guidés pour vous permettre de lancer votre projet sans pépins.

## [Tutos explicatifs pour débuter](./tutos-setup-projets/README.md)

Après avoir mis en place votre environnement de développement, vous retrouverez quelques guides sur l'utilisation d'outils de développement. Elles sont en général destinées à toutes celles et ceux qui codent : chercheurs, ingénieurs SED, thésards.

## [Astuces pour le développement](./astuces-dev/README.md)

Ils vous permettront de mieux comprendre les outils de développement et de sûrement de vous simplifier la vie.

## git
* [Comment enlever un fichier de l'historique de git(EN)](git-repo-cleaning.md)

## gitlab/ci

* [Trucs et astuces gitlab (EN)](gitlab-tricks.md)
* [Trucs et astuces ci (EN)](ci-tricks.md)
* [Setup d'un gitlab-runner (EN)](./setup-a-new-gitlab-runner.md)
* [Quelques astuces pour les slaves utilisant des runners (EN)](./gitlab-runner-tricks)
* [Utilisation des artifacts et publication d'artifacts privés sur repo-sam.inria.fr (EN)](gitlab-artifacts.md)

## vscode/github

* [Comment développer à distance, via un navigateur web](remote-vscode-session.md)

## SED memos

Les articles ci-dessous sont plutôt des mémos destinés aux gens du SED

* [Faire une release d'un paquet DTK](./releases-dtk.md)
- Extension du cas général : [Release d'un paquet DTK avec un label](./releases-dtk-label.md)
- [Build chain pour DTK](./dtk-migration-to-gitlab-runners.md)

* [Démarrer un nouveau projet [EN] (**à nettoyer)**](./start-a-new-project.md)

* [Résumé des documentations: git, gitlab, conda.. (**à nettoyer**)](./our-tools-digest.md)

* [Astuces avancées avec conda](./conda-tricks.md)

* [Astuces spécifiques à des projets](./project-specific-tricks/README.md)
