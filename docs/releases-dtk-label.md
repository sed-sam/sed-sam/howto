[Retour à l'index](./README.md)

# Remarques prélimminaires

1. __Version temporaire jusqu'à ce que le processus soit automatisé__
2. Un même paquet ne peut être associé qu'à un seul label, d'où l'utilisation de `--force` au moment de l'upload : on supprime le paquet de `main`, le label par défaut, et on enregistre dans le label souhaité.


# Étapes à suivre pour faire une release de dtk-xxx avec un label

Pour commencer, suivre les étapes décrites pour la [livraison dans "main" (sans label)](./releases-dtk.md).

## Paquet pour Mac

```bash
ssh ci@inextremis-zie
# Sur inextremis-zie :
cd workspace/${PACKAGE_NAME}-conda-mac/
git checkout master
conda mambabuild . -c dtk-forge/label/qt-5.15 -c conda-forge
# Si tout s'est bien passé :
anaconda upload --user dtk-forge -l qt-5.15 --force /Users/ci/miniconda3/conda-bld/osx-64/${PACKAGE_NAME}-*.tar.bz2 # Check there is only one
```

## Paquet pour Linux

```bash
ssh ci@dtk-centos-7.ci
# Sur dtk-centos-7 :
cd /builds/workspace/${PACKAGE_NAME}-conda-linux/
git checkout master
conda mambabuild . -c dtk-forge/label/qt-5.15 -c conda-forge
# Si tout s'est bien passé :
anaconda upload --user dtk-forge -l qt-5.15 --force /builds/miniconda3/conda-bld/linux-64/${PACKAGE_NAME}-*.tar.bz2
```
