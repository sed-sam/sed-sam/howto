[Retour à l'index](./README.md)

# Utilisation de vscode via un navigateur (ou comment se passer d'un serveur jupyter-lab)

## Principe

Sur le même principe que les outils de prise de contrôle
à distance de type `TeamViewer`, l'idée est de lancer sur
un serveur distant (ex: nef) un tunnel qui se connecte à
un serveur externe sur lequel on peut aussi se connecter
via un navigateur web.

L'interface web ainsi proposée dans le navigateur fournit
un environnement de développement _à la vscode_, permettant
ainsi de faire du développement à distance (depuis ce navigateur
web sur des fichiers présents sur le serveur).

## Prérequis

- disposer d'un compte sur github.com (ou d'un compte microsoft)
- avoir un accès (SSH) sur un serveur
- disposer du binaire `vscode` sur ce serveur

Référence: https://code.visualstudio.com/docs/editor/command-line

### Installation globale de vscode sur le serveur

Sur un serveur dont on assure la gestion, il est possible d'installer vscode globalement:

- se rendre sur https://code.visualstudio.com/download
- récupérer le binaire (.deb) correspondant au serveur
- installer avec apt

```
apt install code_1.89.1-1715060508_amd64.deb
```

### Installation locale (homedir) sur le serveur

Sur un serveur dont on n'a pas la maîtrise (ex: frontal de nef),
il faut récupérer le binaire CLI (client line interface) correspondant
à l'OS du serveur

- se rendre sur https://code.visualstudio.com/download
- télécharger le binaire CLI correspondant à la plateforme
- dé-tar-zipper le binaire dans $HOME/bin (ou tout chemin présent dans votre PATH)

## Lancement du tunnel sur votre serveur favori

- se connecter via SSH sur le serveur
- lancer la commande

```
$ code tunnel
```

- le logiciel va demander à se connecter sur github

```
*
* Visual Studio Code Server
*
* By using the software, you agree to
* the Visual Studio Code Server License Terms (https://aka.ms/vscode-server-license) and
* the Microsoft Privacy Statement (https://privacy.microsoft.com/en-US/privacystatement).
*
✔ How would you like to log in to Visual Studio Code?
[GitHub Account]
```

- puis demander à se connecter sur github via un navigateur pour autoriser le tunnel (entrer le code XXXX-YYYY proposé par `code tunnel`)

```
To grant access to the server, please log into https://github.com/login/device and use code XXXX-YYYY
✔ What would you like to call this machine?
[myserver]

[2024-05-23 18:22:18] info Creating tunnel with the name: myserver

Open this link in your browser https://vscode.dev/tunnel/myserver
```

- et après quelques autorisations coté github

```
[2024-05-23 18:23:37] info [tunnels::connections::relay_tunnel_host] Opened new client on channel 2
[2024-05-23 18:23:37] info [russh::server] wrote id
[2024-05-23 18:23:37] info [russh::server] read other id
[2024-05-23 18:23:37] info [russh::server] session is running
[2024-05-23 18:23:38] info [rpc.0] Checking /home/user/.vscode/cli/servers/Stable-dc96b837cf6bb4af9cd736aa3af08cf8279f7685/log.txt and /home/user/.vscode/cli/servers/Stable-dc96b837cf6bb4af9cd736aa3af08cf8279f7685/pid.txt for a running server...
[2024-05-23 18:23:38] info [rpc.0] Downloading Visual Studio Code server -> /tmp/.tmpCkZLy5/vscode-server-linux-legacy-x64.tar.gz
[2024-05-23 18:23:45] info [rpc.0] Starting server...
[2024-05-23 18:23:45] info [rpc.0] Server started
```

- `Poltronesofà. Autentica qualità. Et voilà!`

## comment rendre ce tunnel permanent

Evidemment, la fin de connexion SSH au serveur va arrêter le tunnel.
Pour le rendre permanent, plusieurs solutions:

- utiliser tmux pour garder la session SSH active

```
user@laptop$ tmux new -t myserver (ce qui ouvre une session tmux)

user@laptop$ ssh myserver

user@myserver ~ $ code tunnel
*
* Visual Studio Code Server
*
* By using the software, you agree to
* the Visual Studio Code Server License Terms (https://aka.ms/vscode-server-license) and
* the Microsoft Privacy Statement (https://privacy.microsoft.com/en-US/privacystatement).
*

Open this link in your browser https://vscode.dev/tunnel/myserver

[CTRL-B] d   (pour se détacher du tunnel)
```

- ou lancer le tunnel en mode détaché avec `nohup` (ce qui rend le tunnel insensible à la déconnexion,
ou au changement de réseau WiFi)

```
user@laptop$ ssh myserver

user@myserver ~ $ nohup code tunnel &
nohup: ignoring input and appending output to 'nohup.out'
```

- on peut évidemment combiner les deux (ici avec une session tmux déjà active)

```
user@laptop$ tmux attach -t myserver
user@myserver ~ $ nohup code tunnel &
```
