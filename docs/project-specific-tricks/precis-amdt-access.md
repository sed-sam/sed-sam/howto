[Retour à l'index](./README.md)


# Access PRECIS amdt resources

## gitlab

* software : https://gitlab.inria.fr/coffee/precis
* test data : https://gitlab.inria.fr/coffee/precis-data
* plugins : https://gitlab.inria.fr/coffee/precis-plugins https://gitlab.inria.fr/coffee/precis-recipes
* documentation : https://coffee.gitlabpages.inria.fr/precis/

## com channels

[**see corresponding file in our private gitlab repo.**](https://gitlab.inria.fr/sed-sam/sed-sam/howto/-/tree/master/docs-private/project-specific-tricks/precis-amdt-access.md)

## CI precis 

https://ci.inria.fr/project/precis-amdt
access granted for coffee and sed team members

## slave CI Fedora29

Configure your ssh client : https://ci.inria.fr/project/precis-amdt/slaves

[**see corresponding file in our private gitlab repo.**](https://gitlab.inria.fr/sed-sam/sed-sam/howto/-/tree/master/docs-private/project-specific-tricks/precis-amdt-access.md)

## CI token for gitlab

"token associated to the GitLab CI Service URL" ; can be read from jenkins > (slave name) > "ce qui declenche le build" > advanced > secret token :
* jenkins job secret token for precis-mac-10-13 slave :  [**see corresponding file in our private gitlab repo.**](https://gitlab.inria.fr/sed-sam/sed-sam/howto/-/tree/master/docs-private/project-specific-tricks/precis-amdt-access.md) 
* jenkins job secret token for precis-fedora-29 slave :  [**see corresponding file in our private gitlab repo.**](https://gitlab.inria.fr/sed-sam/sed-sam/howto/-/tree/master/docs-private/project-specific-tricks/precis-amdt-access.md)

## gitlab account for CI

* gitlab username [**see corresponding file in our private gitlab repo.**](https://gitlab.inria.fr/sed-sam/sed-sam/howto/-/tree/master/docs-private/project-specific-tricks/precis-amdt-access.md)
* gitlab password : [**see corresponding file in our private gitlab repo.**](https://gitlab.inria.fr/sed-sam/sed-sam/howto/-/tree/master/docs-private/project-specific-tricks/precis-amdt-access.md)

