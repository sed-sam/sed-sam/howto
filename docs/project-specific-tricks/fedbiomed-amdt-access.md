[Retour à l'index](./README.md)


# Access FEDBIOMED amdt resources

## gitlab

* software
  * server https://gitlab.inria.fr/fedbiomed
  * client https://gitlab.inria.fr/fedbiomed-node
* documentation : 
  * https://fedbiomed.gitlabpages.inria.fr

## com channels

[**see corresponding file in our private gitlab repo.**](https://gitlab.inria.fr/sed-sam/sed-sam/howto/-/tree/master/docs-private/project-specific-tricks/precis-amdt-access.md)

## CI  

https://ci.inria.fr/project/fedbiomed
access granted for epione and sed team members

## slave CI 

Configure your ssh client : https://ci.inria.fr/project/fedbionet/slaves

 [**see corresponding file in our private gitlab repo.**](https://gitlab.inria.fr/sed-sam/sed-sam/howto/-/tree/master/docs-private/project-specific-tricks/fedbiomed-amdt-access.md)

## CI token for gitlab

* gitlab API token  [**see corresponding file in our private gitlab repo.**](https://gitlab.inria.fr/sed-sam/sed-sam/howto/-/tree/master/docs-private/project-specific-tricks/fedbiomed-amdt-access.md)
* CI secret token  [**see corresponding file in our private gitlab repo.**](https://gitlab.inria.fr/sed-sam/sed-sam/howto/-/tree/master/docs-private/project-specific-tricks/fedbiomed-amdt-access.md)

## gitlab account for CI

* gitlab user  [**see corresponding file in our private gitlab repo.**](https://gitlab.inria.fr/sed-sam/sed-sam/howto/-/tree/master/docs-private/project-specific-tricks/fedbiomed-amdt-access.md)
* gitlab password : [**see corresponding file in our private gitlab repo.**](https://gitlab.inria.fr/sed-sam/sed-sam/howto/-/tree/master/docs-private/project-specific-tricks/fedbiomed-amdt-access.md)

