[Retour à l'index](./README.md)

# Étapes à suivre pour faire une release de dtk-xxx

## dépendances

- si des releases sont à faire pour plusieurs packages dtk-xxx, choisir l'ordre en fonction des dépendances entre ces packages (voir `pkg/env/*.yaml` par exemple)

- par contre, il n'est pas nécessaire de faire systématiquement une release pour tous les dtk-xxx dont dépend notre package.

- bonne pratique : mettre à jour le paquet sur le channel `dtk-forge-staging` pour qu'il ait une version >= celle du channel `dtk-forge`


## release dtk-xxx

- vérifier que les branches locales de *master* et *develop* soient bien à jour
- Faire une nouvelle release de **dtk-xxx** ou hotfix via ```git flow```, par exemple pour une release:

```
git flow release start 2.8.0
```

  - pour une release (nouvelles features/ changement d'API), le **major** et/ou le **minor** sont changés avec le **patch level** à 0 (zéro)
  - pour une release de bugfix/hotfix le **patch level** est incrémenté seul

- mettre à jour le **CHANGES.md** en ajoutant en haut de fichier ce que contient la release (sous la ligne **## NEXT VERSION**)

```
## version 2.8.0 - 2020-04-06
- Add setBounds methods for range parameter and fix bugs
- Register std::array to QMETATYPE system
- ...
```

- mettre à jour la version dans le **CMakeLists.tst** global

```
set(${PROJECT_NAME}_VERSION_MAJOR 2)
set(${PROJECT_NAME}_VERSION_MINOR 8)
set(${PROJECT_NAME}_VERSION_PATCH 0)
```

- commit local des mises à jour avec le commentaire `release x.y.z`
- une fois la release terminée (```git flow release finish x.y.z```):
- ```git push --follow-tags origin develop master``` (normalement, cela pousse les branches develop et master, et le tag)

## package dtk-xxx sur dtk-forge staging

- vérifier que les branches locales de *staging* et *develop* soient bien à jour
- merger la branche locale *develop* dans la branche locale *staging*
- pusher la branche locale *staging*

## conda

- les jobs conda sont exécutés pour les merge requests et branches *develop/master/staging* (sur linux, mac, windows) sur https://ci.inria.fr/dtk, les jobs s'appelent **dtk-XXX-conda-OS** (dtk-XXX est le nom du layer, OS valant linux, mac, win)
- la branche *staging* sert à générer un paquet conda sans faire de release. Il suffit de synchroniser *develop* dans *staging* et de pousser *staging*.
- Les paquets sont déposés sur le channel *dtk-forge-staging*
