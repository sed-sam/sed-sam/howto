# how to create/setup a project gitlab-runner

## install a group gitlab-runner

* create a new slave on ci: https://ci.inria.fr/project/ct/slaves/new

  * 2 CPU
  * 4 GB RAM

* connect to the slave

```
ssh ci@ct-fedora-29.ci
```

* update the OS (395 packages !)

```
sudo -i
dnf update
```


* install Miniconda for the ci account (/builds/miniconda3)

```
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash ./Miniconda3-latest-Linux-x86_64.sh
```

* install a gitlab-runner on ct-fedora-29.ci: https://docs.gitlab.com/runner/install/linux-manually.html

```
sudo -i
curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/rpm/gitlab-runner_amd64.rpm"
rpm -i /root/gitlab-runner_amd64.rpm
which gitlab-runner
```

* change the systemctl script, to use the ci account for the gitlab-runner

  * edit '/etc/systemd/system/gitlab-runner.service'
  * replace user by ci and working directory by '/builds'
  * ExecStart should look like
```
ExecStart=/usr/bin/gitlab-runner "run" "--working-directory" "/builds" "--config" "/etc/gitlab-runner/config.toml" "--service" "gitlab-runner" "--user" "ci"
```
  * validate the modification and restart the service

```
systemctl daemon-reload
systemctl restart gitlab-runner.service
```


* register the runner: https://docs.gitlab.com/runner/register/index.html

  * Obtain a token for a new runner on gitlab
  * for a group runner, go to Settings > CI/CD > Runners
  * click on the top right blue button which gives all instruction. Copyt/Paste the given command as root on the slave
  * give a list of tags (ct, linux, fedora29)
  * choose a **shell** execution

```
# gitlab-runner register --url https://gitlab.inria.fr/ --registration-token GR1XXXXX1234-Abcdefghijklm
Runtime platform                                    arch=amd64 os=linux pid=27188 revision=32fc1585 version=15.2.1
Running in system-mode.

Enter the GitLab instance URL (for example, https://gitlab.com/):
[https://gitlab.inria.fr/]:
Enter the registration token:
[GR1XXXXX1234-Abcdefghijklm]:
Enter a description for the runner:
[fedora29x64]:
Enter tags for the runner (comma-separated):
ct, linux, fedora29
Enter optional maintenance note for the runner:

Registering runner... succeeded                     runner=GR1XXXXX1234-A
Enter an executor: docker+machine, docker, shell, virtualbox, ssh, docker-ssh+machine, kubernetes, custom, docker-ssh, parallels:
shell
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!

Configuration (with the authentication token) was saved in "/etc/gitlab-runner/config.toml"
```

  * the new runner should appear on the runner page of gitlab: https://gitlab.inria.fr/groups/ct/-/runners


* change the gitlab-runner timeout: https://gitlab.inria.fr/help/ci/runners/configure_runners.html#set-maximum-job-timeout-for-a-runner

    * on the CT (global) page
    * select CI/CD > Runners
    * click on the **edit** button in front of the fedora29 runner
    * change the **Maximum job timeout** to 8h
    * verify: https://gitlab.inria.fr/groups/ct/-/runners/5028

* change the project CI/CD timeout: https://gitlab.inria.fr/help/ci/pipelines/settings.md#set-a-limit-for-how-long-jobs-can-run

    * On the left sidebar (of ct/gallery), select Settings > CI/CD.
    * Expand General pipelines (1st entry)
    * In the Timeout field (last field), enter the number of minutes, or a human-readable value like 2 hours -> 8h

## use this runners

in the .gitlab-ci.yaml file, use the tags of the runner (here:  ct, linux, fedora29)

example:
```
weekly:
  stage: build
  tags:
    - fedora29
    - ct
  only:
    - feature/nightly-example-check
    - schedules
  script:
    - conda install -c conda-forge mamba
    - conda activate gallery
    - mamba list
```

You can follow the runner execution from the Pipeline page of the ct-gallery project: https://gitlab.inria.fr/ct/gallery/-/jobs/2255115


## program a scheduled job on gitlab

* goto the project page ()
* CI/CD > Schedules (https://gitlab.inria.fr/ct/gallery/-/pipeline_schedules)
* add a new scheduled job (blue button ''New Schedule), define the frequency ('à la cron' method)


That's all Folks !
