# how to profile a program with Xcode on macOSX


## reference

https://clang.llvm.org/docs/SourceBasedCodeCoverage.html

## compilation

add the '-fprofile-instr-generate -fcoverage-mapping' flags to CXXFLAGS
and recompile

## run

run the binary with:

```
LLVM_PROFILE_FILE="foo_0.profraw" ./bin/dtkLogTests dtkLogCategoryTest
LLVM_PROFILE_FILE="foo_1.profraw" ./bin/dtkLogTests dtkLogLevelTest
LLVM_PROFILE_FILE="foo_2.profraw" ./bin/dtkLogTests dtkLogDestinationSlotTest
```

## merge and format the profiled data

- add /Library/Developer/CommandLineTools/usr/bin to the PATH

```
PATH=/Library/Developer/CommandLineTools/usr/bin:${PATH}
```

```
llvm-profdata merge -sparse foo_*.profraw -o foo.profdata
```

## read the report

```
llvm-cov show ./bin/dtkLogTests -instr-profile=foo.profdata
```

## file level summary

```
llvm-cov report --summary-only ./bin/dtkLogTests -instr-profile=foo.profdata
```

## filter to only get the src files

```
llvm-cov  show  -instr-profile=foo.profdata ./bin/dtkLogTests  --ignore-filename-regex=/Users/jls/Development/dtk/dtk-log/tst
```
