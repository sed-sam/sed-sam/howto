[Retour à l'index](./README.md)


# some shared ci tricks


## ci/gitlab

https://iww.inria.fr/sed-sophia/gitlab-inria-frci-inria-fr-integration/


## windows

* the preferred image is **Community Templates / win10-ssh-conda**

* if the command conda is not found by the CI script, update the PATH environment variable with these steps:
* [ ] Start Menu / Settings
* [ ] Search for "variable", choose "Edit environment variables for your account" "Modifier les variables d'environnement pour votre compte"
* [ ] Select Path, click Modify
* [ ] Click New, set the value to "%USERPROFILE%/Miniconda3/condabin"

* conda is a script and should be called with **CALL** in the build script

```
CALL conda env update -f ./pkg/env/toto.yaml
```



## macosx


* if you cannot connect to a macOSX slave with ssh, add "Ciphers aes256-ctr" in your **.ssh/config** file

* the preferred image to use is: **Community Templates / CI-MacOSX-Catalina-10.15.4-xcode**



## linux

* it works like clockwork

## generic

### give wider read access to CI logs

To open read access to CI logs of a project :
* on the CI project dashboard edit *Configure global security > Authorization > Matrix based security*
* for giving read access to all CI users click *Authenticated users > Overall > read* et *Authenticated users > Job > read*
* for giving read access to everyone from Internet without auth click *Anonymous users > Overall > read* et *Anonymous users > Job > read*

