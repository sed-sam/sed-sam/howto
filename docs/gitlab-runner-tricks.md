[Retour à l'index](./README.md)

# a collection of few gitlab-runner tricks

## launching gitlab-runner

- how to start on a "no display" macOSX slave (@Erwan has the trick :))


## QT tricks

### locale

- Qt need UTF-8 locale

   - on centos: create a /etc/environment containing

```
# cat /etc/environment
LANG=en_US.utf-8
LC_ALL=en_US.utf-8
```

### Qt graphical tests

- to avoid windows to popup (which is not possible on a slave), run the graphical tests with:

```
QT_QPA_PLATFORM=offscreen ctest -T test
```


### macOSX

- then using conda provided Qt packages, on macOSX, use this to avoid `cmake` error messages


```
QT_HOST_PATH=${CONDA_PREFIX} cmake ..
```


## compilation tricks

### stacked environments

- provide a "build environment" containing required packages (eg: make, cmake) and required compilers (eg: up to date C++ compiler) in a specific condsa environment. Then stack the environment of the compiled package.

Exemples can be found on dtk packages (with dtk-build-env), bci-p300/bci-vizapp (with bci-build-env), etc...

The gitlab-ci.yml file looks like:

```
build-linux:
  extends: .build-base
  tags:
    - centos
  script:
    - echo "== build-linux"
    - source /home/gitlab-runner/.bashrc
    - conda env update --prune -f pkg/env/$CI_PROJECT_NAME.yaml
    - conda activate bci-build-env
    - conda activate --stack $CI_PROJECT_NAME
    - rm -fr build
    - mkdir build
    - cd build
    - cmake ..
    - make
    - make install
  artifacts:
    paths:
      - build/
```


## debug tricks

### debug a failing job

Since the compilation phases are not presistent, if a job fails on a slave, the simpliest way to debug is to emulate the job manually on the slave:

- connect to the slave, whihc gitlab-runner identity using ssh (must have put your ssh public key in `~gitlab-runner/.ssh/authorized_keys`
- git clone the repo/branch to debug
- run the commands of the .gitlab-runner section corresponding to the job to debug

```
ssh -A gitlab-runner@my_slave.ci
mkdir Devel
cd Devel
git clone git@gitlab.inria.fr:group/repo.git
cd repo
conda activate whatever
mkdir build
cd build
cmake ..
....
```
