[Retour à l'index](./README.md)


# some shared gitlab tricks


## ci/gitlab

https://iww.inria.fr/sed-sophia/gitlab-inria-frci-inria-fr-integration/


## kindly ask jenkins to build the code

* in a gitlab issue or merge request, when you want to ask Jenkins/ci to build the branch, send this in a comment:

```
Jenkins please retry a build
```

* required : Gitlab *Settings > Integration > Project hooks > Edit > Comments* is enabled
* some projects may configure non-default string through Jenkins project : *Configure > ... > Enabled GitLab triggers > Comment (regex) for triggering a build*

## closing an issue from a merge request

* in a gitlab merge request, when you want to close a corresponding issue (#3351 in the example), you can write this in the **first post** . Various keywords works, in paricular this two:

```
Closes #3351
Fix #123
```

## to avoid Jenkins to build

* start the merge request name with **WIP:**

```
WIP: this merge request is full of it
```
