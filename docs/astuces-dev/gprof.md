[Retour à l'index](./README.md)

Ca serait bien que quelqu'un fasse un tuto sur comment utiliser GProf, l'outil de profiling GNU qui permet de voir combien de temps vous passez dans telle ou telle fonction C++.

D'après Wikipédia : [https://fr.wikipedia.org/wiki/Gprof](https://fr.wikipedia.org/wiki/Gprof)

> Lors de la compilation et de l'édition de liens d'un code source avec gcc, il suffit d'ajouter l'option -pg pour que, lors de son exécution, le programme génère un fichier gmon.out qui contiendra les informations de profilage.
> 
> Il suffit ensuite d'utiliser gprof pour lire ce fichier, en spécifiant les options.

Donc c'est tout bête et il faudrait juste rajouter cette option dans les CMakeLists.

Apparemment Callgrind (de la toolsuite Valgrind) permet d'avoir des infos très poussées, au prix d'une exécution très lente. Il y aurait du coup des outils graphiques come KCacheGrind pour visualiser... mais je n'ai rien fouillé de ce côté.