[Retour à l'index](./README.md)

Ce tutoriel peut être suivi avec :
- ✅ Windows
- ✅ WSL
- ✅ macOS
- ✅ Linux

Visual Studio Code (VSCode pour les intimes) est un "environnement de développement léger" : c'est globalement un éditeur de texte avec plein de fonctions sympas autour pour coder. Allons-y !


![](https://media1.tenor.com/images/1eb62e28b6b72ee989992c3b3da84ad7/tenor.gif?itemid=19813062)

*Cap sur VSCode ! Youhou !*

## 1. Installer Visual Studio Code

Bon c'est trop long maintenant je vais écrire VSCode.

[Téléchargez VSCode ici.](https://code.visualstudio.com/Download) Et installez le !

Une fois ouvert, vous serez accueilli par une fenêtre avec une barre à gauche avec des icones. Cliquer dessus va déployer le panneau associé.

![](./images/hands-on/vscode.jpg)

1. Navigateur de fichiers: vous pouvez voir les fichiers de votre projet
2. Rechercher-remplacer dans les fichiers du projet: vous pouvez faire des recherches en filtrant les fichiers, ou même avec des regex.
3. Git: vous pouvez faire des commits, changer de branche, etc.
4. Debugger: quand vous aurez défini vos cibles de lancement, vous pourrez utiliser VSCode pour debugger votre code ! Ajouter des *points d'arrêt*, voir le contenu de vos variables, progresser ligne par ligne...
5. *Si vous utilisez Windows avec WSL vous pouvez peut-être voir cette icone qui vous liste les fichiers du WSL.*
6. Gestionnaire d'extensions : vous pouvez adapter VSCode à votre projet. Ca va devenir vite essentiel !

Gardez un oeil sur la barre violette en bas de la fenêtre. Elle va se peupler de boutons bien pratiques suivant ce que vous faites.

On va avoir besoin de quelques petits trucs bonus.

## 2. Installer des extensions

Commençons dès maintenant à installer des extensions pour préparer le terrain. Cliquez dans le Gestionnaire d'extensions, et dans le petit champ de recherche, cherchez et installez les extensions dont vous avez besoin.

Si vous avez un projet C++ installez les extensions:
- **C/C++** *de Microsoft*
- **CMake** *de twxs* (coloration syntaxique des fichiers CMake)

Si vous voulez compiler du C++ avec un projet CMake par l'interface de VSCode, vous pourriez vouloir installer l'extension **CMake Tools** de Microsoft *(facultatif)*. **Je ne le conseille pas cependant**:
    * Si vous êtes sous Windows, utilisez plutôt Visual Studio 2019 pour compiler, il est plus facile à configurer.
    * Si vous êtes sous macOS ou Linux, c'est plus facile de compiler avec la ligne de commande.
    * j'ai la flemme d'expliquer comment on configure cette extension.

- Si vous utilisez Windows, vous pouvez utiliser Visual Studio 2019 qui est plus facile à configurer.
- Si vous utilisez Linux ou macOS, compiler avec la ligne de commande est peut-être plus facile pour vous.

Si vous avez un projet Python, installez l'extension **Python** *de Microsoft*.

## 3. La configuration de VSCode : le dossier `.vscode` et la Command Palette

On va pouvoir ajouter une configuration spécifique à votre projet avec VSCode.

Ouvrez le dossier de votre projet avec VSCode. 

Vous pouvez faire appel à toutes les commandes de VSCode à l'aide de la **Command Palette**. Un seul raccourci à retenir

- Pour Windows et Linux : **CTRL + MAJ + P** 
- Pour macOS : **COMMAND (⌘) + MAJ (⇪) + P**. 

![](./images/hands-on/commandpalette.jpg)


Avec la Command Palette, vous pouvez par exemple :
- ouvrir un terminal intégré directement dans la moitié basse de VSCode,
- lancer la prévisualisation d'un fichier Markdown,
- éditer la configuration de votre projet C++,
- sélectionner un environnement conda dédié à votre projet Python,
- faire un commit, une branch, un stash, n'importe quoi lié à Git,
- éditer les préférences de VSCode
- FAIRE BEAUCOUP TROP DE TRUCS.

Il suffit de taper le début de ce que vous voulez faire, et les options sont immédiatement filtrées. Essayez pour voir !

Lorsque vous enregitrez la configuration d'un projet, VSCode va créer un petit fichier de configuration avec l'extension **.json** dans un dossier nommé **.vscode**. Vous verrez dans la suite que les fichiers sont créés comme ça.

## 4. Pour conclure

Vous avez installé VSCode, il permet de lire et d'éditer votre code plus agréablement. La **Command Palette** vous permet de trouver tout ce que vous voulez faire très rapidement, et même de décourir des fonctionnalités de VSCode en tapant le début d'un mot ! Dans la suite on va configurer VSCode pour qu'il prenne en charge le code C/C++ ou Python (ça se fait un peu de la même façon, si vous comprenez comment l'un marche, vous comprendrez l'autre sans soucis !)

## Lire la suite : [Configurer la complétion automatique "Intellisense" de Visual Studio Code](./intellisense-vscode.md)

![](https://media1.tenor.com/images/ebbbb290bfce9e9b047f911f854b873a/tenor.gif?itemid=18404561)

*a !!! (ça veut dire : hypé pour la suite !)* 