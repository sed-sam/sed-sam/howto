[Retour à l'index](./README.md)

Ce tutoriel peut être suivi avec :
- ✅ WSL
- ✅ macOS
- ✅ Linux
- 🔶 Windows : <span style="color:orange">Python seulement</span>

Ce tutoriel ne peut pas être suivi avec :
- ❌ <span style="color:red">Windows (build C++ natif)</span> 


<span style="color:red"><strong>Pour WSL, si ça ne vous parle pas plus que ça, vous devriez suivre le tuto ["debugger  VS2019 avec WSL"](../tutos-setup-projets/windows/advanced-debugging-vs2019-wsl.md).</strong> Il vous permettra d'installer le WSL et d'appréhender le debugging dans VS2019 avec WSL avant d'essayer avec VSCode.</span>

VSCode repose sur des *cibles de lancement*. On définit dans un fichier de configuration l'exécutable et ses arguments. On va voir maintenant comment rentrer ça. VSCode va nous aider à remplir tout ça !

<img src="https://media1.tenor.com/images/412090cceda21666e8135ae11c3c5341/tenor.gif?itemid=20086440" height="300px"/>

*Chaud patate on va enfin pouvoir debugger !*

## 1. Créer des cibles de lancement

Pour rappel, voici les éléments de l'interface.

![](./images/hands-on/vscode.jpg)

Ce qui nous intéresse, c'est l'élément **4**: les configurations de lancement. Facile à se souvenir : un bouton play, et un petit insecte (**bug**) ! Cliquez dessus, et cliquez sur **Create a launch.json file**. Pour l'exemple on va voir comment faire **pour un projet Python et pour un projet C++.**

![](./images/debugger/create-launch-conf.jpg)


### 1 - A. Cibles de lancement pour Python

Pour les projets Python, en cliquant sur **Create a launch.json file**, la *Command Palette* s'ouvre. Vous pouvez debugger certaines cibles pré-enregistrées, mais pour nous, nous allons debugger un **Python File**. Vérifiez que vous avez au préalable bien sélectionné votre environnement conda avec le bon Python (voyez l'ampoule sur le screenshot !)

![](./images/debugger/palette-debug-python.jpg)

VSCode va créer pour vous un fichier **launch.json** dans le dossier **.vscode**. Par défaut vous avez ce contenu : 

```json
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Python: Current File",
            "type": "python",
            "request": "launch",
            "program": "${file}",
            "console": "integratedTerminal"
        }
    ]
}
```

C'est une **cible de lancement**. Si vous l'exécutez, actuellement elle lancera le fichier Python que vous avez ouvert et que vous êtes en train d'éditer dans VSCode. On va l'éditer pour spécifier un fichier en particulier avec des paramètres en ligne de commande.
- on change la clé `name` pour un truc plus sympa
- on change la clé `program` pour spécifier ce qu'on veut exécuter, ici un fichier .py dans le dossier du projet (`${workspaceFolder}`).
- on peut rajouter des arguments avec la clé `args`

Par exemple, si je lançais d'habitude la commande `python populate_mnist.py --node_address=http://127.0.0.1:5000 --node_id thedata`, la configuration ça donnera ça. **Notez que les arguments sont donnés sous forme de liste : si d'habitude vous mettez des espaces pour votre commande, séparez-les comme les éléments d'une liste à la place.**


```json
...
{
    "name": "Populate Node 1",
    "type": "python",
    "request": "launch",
    "program": "${workspaceFolder}/populate_mnist.py",
    "args": [
        "--node_address=http://127.0.0.1:5000",
        "--node_id", 
        "thedata"
    ],
    "console": "integratedTerminal"
}
...
```

Vous pouvez créer comme ça autant de cibles que vous voulez !

On peut passer à la partie 2 (pour le fonctionnement général du debugger, c'est pareil pour C++ et Python).

### 1 - B. Cibles de lancement pour C++

Supposons que vous avez compilé votre programme C++ par vos propres moyens, dans votre terminal. 

Supposons donc que :
- vous avez compilé votre binaire dans un dossier appelé `build`, lui-même dans le dossier de votre projet.
- vous avez compilé en mode **Debug** ou **RelWithDebInfo** avec CMake.
- vous avez donc le binaire nommé `mon_binaire_compile` dans le dossier `build/bin/mon_binaire_compile`

Pour les projets C++, en cliquant sur **Create a launch.json file**, la *Command Palette* s'ouvre. 

**Suivant si vous utilisez macOS ou Linux ou le WSL, l'affichage de la partie suivante peut être un peu différent. Retenez les points suivants :**

- Pour macOS, on debug avec **LLDB**.
- pour Linux et WSL, on debug avec **GDB**.
- Pour le WSL, VSCode va vous proposer si vous voulez debugger un programme Windows natif ou un programme WSL avec GDB. (voir screen ci-dessous, **(1)**) Le debugging natif Windows n'est pas possible à cause de **conda**, donc on sélectione **C++ (GDB/LLDB)**. 
- S'il vous propose plusieurs types de configuration suivant votre compilateur vous demandant de lancer le fichier courant (voir screen ci-dessous, **(2)**), sélectionnez simplement **Default Configuration (2)**.

VSCode va vous créer un fichier **launch.json**, soit vide, soit déjà un peu pré-rempli. S'il est vide, pour rajouter une cible, cliquez sur **Add Configuration (3)**, puis sélectionnez **C/C++: (gdb) Launch (4)** (pour Linux, sinon pour macOS c'est pareil mais avec **lldb** au lieu de **gdb**.)

En fait, on veut créer une configuration où on demande au debugger GDB/LLDB de lancer un programme particulier.

![](./images/debugger/cpp-debug-conf.jpg)

VSCode vous rajoute donc un bloc de configuration. Editez-le pour lancer votre programme ! <span style="color:red">Changez :</span>
- `name`: un petit nom sympa pour votre cible
- `program` : la cible à exécuter. Si on suit l'exemple ci-dessus, ça pourrait être `${workspaceFolder}/build/bin/mon_binaire_compile`.
- `args`: c'est la liste des arguments.
- `cwd`: c'est le *Current Working Directory*, le dossier qui est pris en compte quand l'exécutable se lance (par exemple ça donnera la base des chemins relatifs si votre programme doit ouvrir des fichiers...)
- `environment` : c'est pour spécifier des variables d'environnement au moment de l'exécution du programme.

Par exemple, si d'habitude vous lancez votre programme comme ceci : 

```bash
jolevy@SMARA:~/sw2d/build$ ./bin/mon_binaire_precompile -a ../example/file.txt
```

Vous pouvez écrire ça comme ça. En spécifiant `"cwd": "${workspaceFolder}"`, vous pouvez directement faire référence à `./example/file.txt`. **Notez que les arguments sont donnés sous forme de liste : si d'habitude vous mettez des espaces pour votre commande, séparez-les comme les éléments d'une liste à la place.**

```json
{
    "name": "(gdb) Launch",
    "type": "cppdbg",
    "request": "launch",
    "program": "{workspaceFolder}/build/mon_binaire_compile",
    "args": [
        "-a",
        "./example/file.txt"
    ],
    "stopAtEntry": false,
    "cwd": "${workspaceFolder}",
    "environment": [],
    "console": "externalTerminal",
    "MIMode": "gdb",
    "setupCommands": [
        {
            "description": "Enable pretty-printing for gdb",
            "text": "-enable-pretty-printing",
            "ignoreFailures": true
        }
    ]
}
``` 

Vous pouvez créer comme ça autant de cibles que vous voulez !

On peut passer à la partie 2 (pour le fonctionnement général du debugger, c'est pareil pour C++ et Python).

## 2. Lancer une cible de debugging

Placez des *breakpoints* en cliquant à gauche du numéro de ligne dans vos fichiers de code, comme indiqué en (1).

Ouvrez le volet latéral de Debugging, sélectionnez la cible de debugging que vous voulez lancer, et cliquez sur le bouton Play (2). Le debugging se lance !

![](./images/debugger/python-debugging-view.jpg)

La flèche verte indique le breakpoint où le programme s'est arrêté. Il est entouré en jaune, et la ligne est surlignée en jaune.

Voici un récapitulatif des zones indiquées en jaune:
- Zone A : vous donne le type et l'état des variables (le type des variables c'est instructif en Python)
- Zone B : vous donne la liste des *breakpoints*, et vous permet de les désactiver temporairement au besoin
- Zone C : vous donne la pile d'appel des fonctions pour chaque thread
- Zone D : c'est une barre d'outil qui vous permet d'avancer ligne par ligne, de rentrer dans les fonctions ou d'en sortir ou d'arrêter le debugging.

Le fonctionnement du debugger est tout à fait identique pour debugger du C++ ou du Python. Une fois que vos *cibles de lancement* sont définies, vous n'avez qu'à les lancer dans le volet de debugging.

*Note : les breakpoints ne sont stockés ni dans le fichier de configuration launch.json, ni dans les fichiers de code. Ils ne sont pas sauvegardés : si vous fermez VSCode, ils disparaitront. En fait, on n'utilise le debugger que lorsqu'on va se lancer à la recherche d'un crash ou d'un fonctionnement particulier, c'est pour les

## 3. Contrôler le *flow* d'exécution

On va s'intéresser à la bare d'outil pointée ci-dessus en Zone D. Elle a 5 icones, en passant la souris dessus vous verrez leur nom. Elles sont utiles quand votre programme s'arrête sur un *breakpoint*.

![](./images/debugger/debug-bar.jpg)

Dans l'ordre:
- **Continue** : l'exécution continue, jusqu'au prochain breakpoint
- **Step Over** : exécute la prochaine ligne dans la fonction où vous êtes.
- **Step In** : si la ligne où vous êtes arrêté fait appel à une fonction, "Step In" vous fait *rentrer dans la fonction* pour l'exécuter ligne par ligne !
- **Step Out** : si vous debuggez dans une fonction avec "Step Into", "Step Out" vous fait *sortir de la fonction pour continuer l'exécution*. En ça, très souvent "Step Out" revient à la même chose que "Continue".
- **Relaunch** : ça arrête le debugging et ça relance le truc depuis le début
- **Stop** : ben... ça arrête le debugging.


## 4. La question à mille euros : le dilemme du commit

On a créé des cibles de debugging dans VSCode. Ces cibles sont stockées dans un fichier **.vscode/launch.json**. Nous voilà donc à la question à mille euros : *dois-je commit et push ce fichier de configuration ?*

La réponse : même si historiquement les gens ont tendance à ne pas commit ces fichiers de configuration, c'est vraiment relou de les refaire quand vous re-clonez le projet depuis Gitlab. Mais comme on l'a vu, on peut faire autant de *cibles de debug* qu'on veut, il suffit de recopier la configuration, de lui donner un nouveau nom et de l'adapter.

Vous pouvez donc faire des cibles de debug pour telle ou telle personne qui utilise macOS, ou Linux, ou qui crée son dossier **build** différemment des autres. Vous pouvez comme ça les nommer, les commit, et les retrouver quand vous le souhaitez, sans pour autant venir modifier celles des autres. 

Donc à lquestion *dois-je commit et push ce fichier de configuration ?*, la réponse est : **oui, vous pouvez, à condition de faire des cibles de debug pour vous pour ne pas modifier celles des autres.**. 

A vous de jouer !

<img src="https://media1.tenor.com/images/710ce072d122d78f9f682c3ba4ddca12/tenor.gif?itemid=19498632" height="300px"/>

## Lire la suite : [Git avec Visual Studio Code](./git-vscode.md)