[Retour à l'index](../README.md)

On recommande Visual Studio Code parce qu'il permet de faire beaucoup de choses et qu'il est disponible pour Windows, macOS et Linux.

## [Prendre en main Visual Studio Code](./hands-on-vscode.md)

## [Configurer la complétion automatique "Intellisense" de Visual Studio Code](./intellisense-vscode.md)

## [Utiliser le debugger graphique de Visual Studio Code (C++, Python...)](./debugger-vscode.md)

## [Git avec Visual Studio Code](./git-vscode.md)

## [Quelques idées sur gprof, le profiler C++](./gprof.md)

