[Retour à l'index](./README.md)

Ce tutoriel peut être suivi avec :
- ✅ Windows
- ✅ WSL
- ✅ macOS
- ✅ Linux

- Prérequis : 
 * avoir une vague idée de ce que c'est Git, un commit, une branche.


Si Git en ligne de commande vous effraie, vous pouvez passer par VSCode pour l'utliser. C'est un peu plus facile, et vous verrez peut-être un peu mieux ce qui se passe.

## 1. L'interface de VSCode pour Git

Dans VSCode, pour ce tuto, ce qui nous intéresse, c'est la 3e icone du panneau de gauche, Git.

En cliquant sur l'icone, le volet des modifications git apparait.

![](./images/git/git-files.jpg)


Les fichiers modifiés sont alors visibles. *S'ils ne le sont pas, actualisez la vue avec l'icone en flèche en rond en haut du volet.*

A côté de chaque fichier il y a une lettre :
- <span style='color:green'>U : nouveau fichier</span>
- <span style='color:orange'>M : fichier modifié</span>
- <span style='color:red'>D : fichier supprimé</span>

En passant la souris, 3 petites icones apparaissent. En passant la souris sur chaque icone, sa fonction s'affiche. De gauche à droite:
- **Icône fichier** : ouvrir le fichier
- **Icone flèche** : annuler tous les changements
- **Icone ➕** : valider les changements (*stage*)

En double-cliquant sur les fichiers dans la vue git, VSCode va vous afficher les modifications apportées "avant-après", comme sur le screen ci-dessus (ce qui est en rouge est enlevé, ce qui est en vert est rajouté, tout simplement).

## 2. Faire un commit et gérer ses branches

Pour faire un commit, vous pouvez faire comme ceci (les numéros font référence au screen ci-dessus)
- ouvrir le volet git
- valider les changements des fichiers qui vous intéressent en cliquant sur l'icone ➕ pour chaque fichier **(1)**
- écrire un message de commit dans la zone de message **(2)**
- et faire le commit en cliquant sur le bouton Valider ✔ **(3)**
- et pousser le commit en cliquant sur le bouton de synchronisation 🔄 **(4)**

Si vous voulez récupérer les derniers changements d'une branche, il suffit de cliquer sur le même bouton de synchronisation 🔄 **(4)**.

Pour passer à une autre branche, regardez le screen ci-dessous. Cliquez sur le bouton des branches juste à gauche du bouton de synchronisation **(1)**. Ca va ouvrir la *Command Palette*. Vous pourrez alors choisir la branche que vous voulez, ou même en créer une nouvelle. **(2)**

![](./images/git/git-branch-choose.jpg)

## 3. Gérer les conflits

Vous pouvez faire à peu près n'importe quelle opération de git avec la *Command Palette*, y compris des *merges*. En cas de conflit, VSCode va vous afficher le conflit et va vous demander quels changements valider.

- pour l'exemple, on suppose qu'on merge la branche **some-branch** dans la branche **master**. On veut donc ajouter les changements faits dans **some-branch** à la branche **master**.
- Les fichiers comportant des conflits apparaissent avec la lettre <span style='color:violet'>C : conflit de merge</span> (zone rouge à gauche)
- vous êtes invités à sélectionner quels changements vous voulez garder (zone verte):
    * *Accept current changes* : garder les modifications déjà dans la branche d'origine du merge, dans notre exemple c'est **master**,
    * *Accept incoming changes* : garder les modifications apportées par la branche qu'on veut ajouter, c'est-à-dire la branche **some-branch**
    * *Accept both* : garder les 2 changements.

![](./images/git/merge-conflict.jpg)

Vous avez tout à fait le droit de continuer à modifier le fichier avant de valider le merge.

Une fois que vous avez décidé quels changements garder, pensez à sauvegarder votre fichier. Ensuite, vous n'avez plus qu'à *valider* les changements de votre fichier et des autres fichiers du merge avec l'icone ➕ dans le volet latéral de git, et faire le commit pour valider le merge.


Bon je suis allé un peu vite là dessus, mais j'espère qu'avec un peu de pratique vous saurez vous en sortir.