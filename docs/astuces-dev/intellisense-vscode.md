[Retour à l'index](./README.md)

Ce tutoriel peut être suivi avec :
- ✅ Windows
- ✅ WSL
- ✅ macOS
- ✅ Linux

L'outil de complétion automatique et d'analyse de code de VSCode porte un p'tit nom sympa : *Intellisense*. Ca permet de vérifier que votre code n'a pas d'erreurs, que les inclusions sont bien prises en compte, et ça vous permet de compléter automatiquement vos fonctions et variables ! C'est vraiment sympa quand on code tous les jours dans VSCode.

## 1. Configurer son projet 

On traitera ici de Python et pour les projets C++. Si vous utilisez un autre langage, il va falloir chercher par vous-même quelle extension de VSCode est adaptée et comment la configurer.

### 1 - A. Configurer un projet Python

Si vous utilisez Python au SED, il y a toutes les chances que vous utilisez **conda**. Il vous permet de créer des environnements séparés pour installer des paquets binaires et des paquets Python.

- D'abord, ouvrez le dossier de votre projet Python. 
- Ouvrez la **Command Palette** (souvenez-vous : pour Windows et Linux: **CTRL + MAJ + P**, pour macOS : **Command (⌘) + MAJ (⇪) + P**). 
- Dedans, tapez "**Python**", puis sélectionnez "**Python: Select Interpreter**". C'est la commande liée à l'extension **Python de Microsoft** que vous avez installée au tutoriel précédent.
- La Command Palette va vous proposer tous les environnements qu'elle trouve, y compris tous les environnements conda. Il n'y a plus qu'à choisir celui qui vous convient ! 

Vous vous souvenez de la barre en bas de la fenêtre ? Elle affiche maintenant l'environnement conda configuré pour votre projet. (Sur le screen ci-dessus j'avais déjà choisi un environnement donc elle est déjà affichée.)

![](./images/intellisense/python-env.jpg)

Maintenant, VSCode va prendre en compte l'environnement Python que vous avez choisi lorsque vous tapez du code Python.

Essayez maintenant d'ouvrir un terminal intégré : 
- Ouvrez la Command Palette
- tapez "**terminal**"
- Sélectionnez: **View: Toggle integrated terminal**
- Constatez : un terminal s'ouvre en bas de votre fenêtre... *et les commandes pour activer votre environnement conda spécifique sont tapées automatiquement pas VSCode.*

Bon ! Passons à un projet C++.

### 1 - B. Configurer un projet C++

Si vous avez un projet C++ au SED, il y a *aussi* toutes les chances que vous utilisez **conda** aussi ! Malheureusement, VSCode ne peut pas trouver automatiquement les dépendances de votre projet C++ (les includes .h en particulier). Sélectionnez un environnement conda avec l'extension Python, ça ne marche que pour les projets Python !

<img src="https://media1.tenor.com/images/2a0a2a1021e29f96173e35bc17f5b326/tenor.gif?itemid=18015941" height="300px"/>

*VSCode quand on lui dit pas où sont les includes pour le C++, il est paumé.*

Il va falloir dire explicitement à l'extension **C/C++** où les dépendances se trouvent, donc il va falloir lui donner le chemin vers votre environnement conda.

- D'abord, ouvrez le dossier de votre projet C++. 
- Ouvrez la **Command Palette** (souvenez-vous : pour Windows et Linux: **CTRL + MAJ + P**, pour macOS : **Command (⌘) + MAJ (⇪) + P**). 
- Dedans, tapez "**C/C++ Edit **", puis sélectionnez "**C/C++: Edit Configuration (UI)**". C'est la commande liée à l'extension **C/C++ de Microsoft** que vous avez installée au tutoriel précédent.

![](./images/intellisense/ccpp.jpg)

L'interface de configuration de l'extension C/C++ s'ouvre. Vous pouvez modifier les options si vous savez ce que vous faites. Maintenant regardez la section **Include le chemin**.

![](./images/intellisense/cpp-config.jpg)

Ici, on va rajouter le chemin vers notre environnement conda. Dans cette section, je suppose que vous avez installé **Miniconda3** dans le chemin par défaut. <span style="color:red">Si ça n'est pas le cas, adaptez le chemin pour votre environnement conda !</span>

- **Si vous utilisez Windows**, rajoutez ceci, et changez **le_nom_de_votre_environnement**.

```
${workspaceFolder}/**
${userProfile}/Miniconda3/envs/le_nom_de_votre_environnement/include/**
${userProfile}/Miniconda3/envs/le_nom_de_votre_environnement/Library/include/**
```

- **Si vous utilisez macOS**, rajoutez ceci, et changez **le_nom_de_votre_environnement**.

```
${workspaceFolder}/**
${userProfile}/opt/miniconda3/envs/le_nom_de_votre_environnement/include/**
```


- **Si vous utilisez Linux**, rajoutez ceci, et changez **le_nom_de_votre_environnement**.

```
${workspaceFolder}/**
${userProfile}/miniconda3/envs/le_nom_de_votre_environnement/include/**
```

Avec cela, VSCode va analyser les includes contenus dans votre environnement conda et il va analyser les fichiers à sa portée. 

<img src="https://media1.tenor.com/images/7a546555041362b2eb423ef8e029cde7/tenor.gif?itemid=17902545" height="300px"/>

*Il analyse. Ca peut prendre un peu de temps !*

## 2. Les bienfaits d'Intellisense

Une fois que vous avez configuré Intellisense pour votre projet vous avez pas mal de features sympas :
- passer votre souris sur les fonctions/variables va vous donner des infos dessus
- passer votre souris en gardant MAJ appuyé vous permet d'avoir des infos détaillés, et en cliquant sur les classes/fonctions/variables vous irez directement à leur définition
- VSCode va vous suggérer les méthodes des classes quand vous les tapez
- VSCode va vous souligner les erreurs
- et plein de détails sympas ! C'est tellement *satisfaisant !*

![](https://media1.giphy.com/media/qKZbwTZAPAHkKgndM5/giphy.gif)

*Je suis satisfait et vous l'êtes sûrement aussi.*

## 3. La question à mille euros : le dilemme du commit

On a vu, ces fichiers de configuration en JSON sont créés dans le répertoire **.vscode**. Bon, donc la question à mille euros : **dois-je commit et push ces fichiers de configuration ?**

Eh bien c'est compliqué. *D'habitude, il est de bon aloi de ne pas les commit dans git.* Par égard envers les utilisateurs qui auraient un setup différent du votre, ça évite que chacun commit ses propres modifications des fichiers de configuration. Mais ça veut dire que si vous pensez supprimer le projet et le re-cloner depuis Gitlab, il va falloir refaire ces quelques étapes de configuration... ça peut être un peu casse-pieds !

Cependant, si vous avec un projet C++ et que vous **voulez vraiment** garder ces fichiers de configuration dans Git, l'extension C/C++ vous permet de créer plusieurs **configurations**. Vous pouvez en faire une à votre nom, et comme ça vous ne vous marcherez pas sur les pieds avec le reste de votre équipe. 

Dans la configuration de l'extension C/C++ (revenez plus haut si vous avez oublié comment on l'ouvre), vous pouvez créer plusieurs profils pour chacun.

![](./images/intellisense/cpp-multiple-configs.jpg)

Vous pouvez donc vous en sortir comme ça. Attention de sélectionner la bonne config quand vous ouvrez la fenêtre !

Je crois qu'on en a fini avec la configuration ! La compilation vous savez déjà faire, normalement !
____________________________

Allez, maintenant, le moment que vous attendez tous.

## Lire la suite : [Utiliser le debugger graphique de Visual Studio Code (C++, Python...)](./debugger-vscode.md)

