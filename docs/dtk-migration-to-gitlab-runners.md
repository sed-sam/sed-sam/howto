# principes/techno des builds dtk

## objectifs

- utiliser les runners de compilation gitlab-runner plutôt que Jenkins
- en profiter pour proposer un schéma de nommage des numéros de version des dépendances
- pour ne pas casser les actuels builds en place sur Jenkins, utiliser le channel `dtk-forge6`, et donc faire passer `dtk` à `Qt6`

## runners

- les runners sont déclarés comme runner globaux du groupe `dtk` de gitlab (https://gitlab.inria.fr/groups/dtk/-/runners)
- ils sont actuellement au nombre de 4, pour chacun des OS suivants:

| OS        | host                  | key       |
| :-------- | :-------------------- | :-------- |
| osx       | dtk-macos-catalina.ci | sRZVDkCbN |
| osx-arm64 | catalysis-zie         | j5KdEV2ss |
| linux     | dtk-centos-7.ci       | 3q8t4dgGP |
| windows   | dtk-win10.ci          | eNDyiy5uY |

Remarques:

  - le runner windows est un peu lent, alors que gonflé à bloc (4 coeurs, 12 gigas de RAM), il est envisagé de le changer et de faire tourner un runner sur showroom-1
  - pour éviter les clashs d'environnement sur les runners partagés (ex: catalysis-zie), il est envisagé de déclarer un compte dédié à `dtk` (plutôt que d'utiliser le compte générique ci, potentiellement utilisé par d'autres projets ou par jenkins)


## jobs/.gitlab-ci.yml

### description des jobs

Il y a deux chaînes de compilation distinctes:

- compilation+ctest lors des `merge request`
- deploy (packaging conda) lors des releases dans la branche `main`

Remarque:

  - afin de ne pas casser les jobs de CI en place, il a été créé la branche `feature/gitlab-runners` pour mettre au point le prototype de build/packaging dans chacun des modules dtk en cours de migration
  - à l'issue du prototypage, les branches `feature/gitlab-runners` seront poussées dans `master` (et donc une nouvelle release sera effectuée)
  - du coup, le contenu des fichiers `.gitlab-ci.yml` devra être adapté:

     - remplacement de `feature/gitlab-runners` par `develop` pour `build`/`ctest`
     - remplacement de `feature/gitlab-runners` par `master` pour `deploy`
     - modifier les tags `needs` pour `needs: []` pour `deploy`

### jobs de type build

- il permet de compiler le module `dtk-*`
- utilise conda
- supprime/ré-installe l'environnement de build conda (`pkg/env/dtk-*.yaml`)
- compile
- installe
- garde l'installation sous forme d'artifact pour le job suivant

### jobs de type ctest

- il permet de lancer les tests
- installe l'environnement conda de build, préparé par le job précédent
- lance les tests
- garde le résultat des tests sous forme d'artifact, de manière à pouvoir visualiser le résultat des tests (ex: https://gitlab.inria.fr/dtk/dtk-log/-/pipelines/847025/test_report)


### jobs de type deploy

- il permet de construire le package conda et de le pousser sur anaconda (dans le channel `dtk-forge6` actuellement)

- on peut vérifier les uploads dans https://anaconda.org/dtk-forge6/repo

### contenu du fichier .gitlab-ci.yml

#### utilisation de variables

- la variable `CI_PROJECT_NAME` permet de rendre les fichiers quasi identiques pour tous les modules `dtk-*`
- les seules modifications à la marge arrivent lorsque:

  - les fichiers d'environnement conda sont OS dépendants (ex: `dtk-distributed`)
  - il n'y a pas de tests (ex: `dtk-fonts`)

- la variable `DTK_FORGE_TOKEN` est utilisée à la phase `deploy` pour pousser le package sur anaconda. La description de la mise en place de cei est décrit plus loin.


#### artifacts

- build: comme les phases de test et de build sont séparées l'artifact permet de garder le résultat de compilation pour ne pas avoir à le refaire en phase de test

- test: l'artifact est le résultat du test et permet de le visualiser dans la page concernant chaque pipelines

- deploy:

  - si on publie le package conda sur anaconda, on n'a pas besoin d'artifact
  - par contre, si on compile un package privé (à pousser ensuite sur repo-sam.inria.fr via le serveur dream.inria.fr), on doit garder un artifact et rajouter une phase de publication en sollicitant le gitlan-runner qui tourne sur dream.inria.fr. Ceci est décrit dans https://gitlab.inria.fr/sed-sam/sed-sam/howto/-/blob/master/docs/gitlab-artifacts.md


#### dtk-build environnement

- pour simplifier les scripts de compil et fixer des versions de compilateurs (typiquement en centos),
  les pré-requis sont installés via l'environnement conda `dtk-build` et "stacké" avant l'environnment du chaque module
- cf: https://gitlab.inria.fr/dtk/dtk-conda-recipes/-/tree/develop/dtk-build

Remarque:
- lors de l'édition des chacun de des environnements dans `dtk-conda-recipes`, l'environnement conda `dtk-build` est mis à jour automatiquement
sur les slaves impactés.

#### spécificité osx/asx-arm

Pour les compilations Qt, besoin de définir la variable QT_HOST_PATH=$CONDA_PREFIX avant de lancer cmake (car il semble que le paquet qt6-main soit cross-compilé)

#### spécificité windows

- les jobs lancent un powershell:

  - les slash(/) et antislash (\) sont supportés
  - le '$' permet la subtitution de variable (et pas le '%TOTO%')
  - certaines commandes sont mieux implémentées que dans le 'cmd' natif de windows:

    - Remove-Item (plutôt que DEL qui a des effets de bord)


## authentification gitlab/anaconda

### création du token sur anaconda.org

- se logguer sur `anaconda.org` en tant que amdt-team
- aller dans le menu `amdt-team` et "switcher" sur le projet concerné (ici: `dtk-forge`)
- aller dans le menu `settings`, cliquer sur `access` dans la rubrique de gauche
- créer un token:

  - `INRIA_GITLAB_UPLOAD`
  - avec **tous** les droits d'accès (première case à cocher)
  - récupérer le token (ex: am-6f8ffd5863-661d-4957-1234-73b72023e1a7)


### enregistrement du token sur gitlab.inria.fr

  - se logguer sur https://gitlab.inria.fr/dtk
  - aller dans le menu `settings` / `CI/CD`
  - cliquer sur `Variables`
  - ajouter la variable `DTK_FORGE_TOKEN` avec le token récupéré ci-dessus
  - choisir l'option `masked`
  - la variable `$DTK_FORGE_TOKEN` est maintenant utilisable dans le fichiers `.gitlab-ci.yml`



## conda

Voici les choix effectués pour le contenu des fichiers d'environnement ou de build conda

### environnement de build (pkg/env)

- on fixe la borne inférieure et la borne supérieure des dépendances
- on ne met que le majeur et le mineur, pas le patchlevel
- exemple:

```
name:         dtk-core
channels:
  - dtk-forge
  - conda-forge
dependencies:
  - dtk-log >=2.5,<3
```

### environnement de packaging (pkg/recipe)

- on fixe **exactement** les dépendances avec les autres packages
- on ne met que le majeur et le mineur, pas le patchlevel
- exemple:

```
- dtk-log  =2.13
- dtk-core =2.5
```

- sauf pour les modules "terminaux" (`dtk-log`/`dtk-font`/`dtk-script`), on ne précise pas la version de `Qt6`

Remarque:

- à chaque release d'un `dtk-*`, il va donc falloir valider le numéro de version **exacte** des dépendances, et éventuellement remonter la chaîne de dépendances pour revalider le tout (ex: changement de version de `dtk-log`, il va falloir modifier les meta.yaml de `dtk-core`, `dtk-core-distributed`, etc... pour les recompiler. Cela ne changera pas leur propre numéro de version, mais augmentera le build number et garantira la cohérence)
- ceci garantit aussi  que quelque soit le moment où l'on va faire un `conda install` d'un logiciel (ex: gnomon) et tant qu'il n'y aura pas eu de release de **ce** logiciel, on tombera toujours sur les même dépendances conda sur `dtk-*`. Alors que dans le même temps, il y aura peut-être eu des release de `dtk-*`

#### tous OS

- version de `cmake > 3.21`
- utilisation de `ninja`

#### linux

- on construit sur la chaîne cdt/centos7  (déployée via `dtk-build-linux.yaml` sur le runner linux)

#### windows

- on construit avec `Visual Studio 16 2019`
- fichier bld.bat:

```
cmake ..  -G "Visual Studio 16 2019" ^
    -Wno-dev ^
```

#### OSX

- on construit sur le SDK 10.15 (pb de `fs:path` utilisé par Qt6 et non fourni en 10.14)


#### fichier pkg/recipe/conda_build_config.yaml

```
compiler_version:          # [linux]
    - 13.2.0               # [linux]
cxx_compiler_version:      # [linux]
    - 13.2.0               # [linux]
cdt_name: cos7             # [linux]
MACOSX_DEPLOYMENT_TARGET:  # [osx and x86_64]
  - '10.15'                # [osx and x86_64]
MACOSX_SDK_VERSION:        # [osx and x86_64]
  - '10.15'                # [osx and x86_64]
```

Remarque: la version des compilateurs linux doit être identique à ce qui est défini dans l'environnement `dtk-build`


## guide pratique: liste des fichiers à vérifier lors du passage de develop -> feature/gitlab-runners

- `.gitlab-ci.yml`: partir du template de `dtk-log`
- `pkg/env/*.yaml`: limiter la liste des dépendances et mettre description bornes inférieures et supérieures
- `pgk/recipe/meta.yaml`: limiter la liste des dépendances et mettre des versions exactes, version de cmake
- `pkg/recipe/bld.bat`: fixer la version de compilateur windows
- `pkg/recipe/conda_build_config.yaml`: SDK macOSX, cdt_cname pour centos7

## guide pratique: release dans master

On a choisi de changer le major du numéro de version lors de ce passage à Qt6 pour le passer à 3.0.0

- merger develop -> feature/gitlab-runners

- mettre à jour:

   - `.gitlab-ci.yaml`: recopier le .gitlab-ci.yaml de `dtk-log`, remplacer develop par feature/gitlab-runners
   - `pkg/env/*.yaml`: passer de dtk-forge6 à dtk-forge, passer aux versions dtk-*/3.0.0
   - `pkg/recipe/meta.yaml`: passer aux versions 3.0
   - `pkg/recipe/conda_build_config.yaml`: recopier celui de dtk-log
   - `pkg/recipe/build.sh`: ajouter QT_HOST_PATH pour osx-arm64

- remarque: ne pas remplacer master par feature/gitlab-runners (dans .gitlab-ci.yml), sinon on va pousser une version numérotée 2.x.y sur anaconda, alors que les dépendances sont en 3.x.y - le (petit) pb est qu'on ne testera le conda-build des packages qu'après le release...

- pousser feature/gitlab-runners et vérifier le pipeline sur les 4 OS

- demander le merge de feature/gitlab-runners -> develop sur gitlab.inria.fr

- vérifier et valider

- faire la release localement:

```
git checkout develop
git pull
git flow init
git flow release start 3.0.0
```

 - mettre à jour:

   - `CHANGES.md`: indiquer les derniers changements
   - `CMakeLists.txt`: numéro de version (3.0.0) et cmake à 3.21
   - `.gitlab-ci.yaml`: remettre develop (supprimer feature/gitlab-runners)

 - terminer:

```
git commit -am 'release 3.0.0'
git flow release finish 3.0.0
git push --follow-tags
git checkout master
git push --follow-tags
git checkout develop
git merge master
git push
```


## modules dtk-*

Etat des lieux des modules `dtk-*`

### liste des dépendances entre modules

- sans dépendance avec d'autres modules `dtk`:

  - dtk-log
  - dtk-script
  - dtk-fonts

- core/nogui:

  - dtk-core       : log
  - dtk-distributed: core / log

- graphiques/gui:

  - dtk-themes: fonts

  - dtk-widgets         : core / log / themes / fonts
  - dtk-visualization   : widgets / core / log / themes / fonts
  - dtk-imaging         : widgets / core / log / themes / fonts
  - dtk-plugins-imaging : imaging / widgets / core / log / themes / fonts
  - dtk-macs            : widgets / core / log / themes / fonts

- python:

  - dtk-core-python     : core / log / script
  - dtk-imaging-python  : imaging / widgets / core / log / themes / fonts / core-python / script


### spécificité dtk-*

- dtk-fonts: suppression des tests
- dtk-core-python: `conda_build_config.yaml` spécifique pour builder pour plusieurs versions de python
- dtk-distributed: environnements conda différent pour windows/mac/osx (MPI), les tests ne passent pas sous windows, pb de 'shallow git clone'


- dtk-widgets
- dtk-visualization
- dtk-imaging
- dtk-imaging-python
- dtk-plugins-imaging
- dtk-macs
- dtk-controls: ???? pkg/env non standard et pour linux seul

### état de la migration et numéros de version associés

Voici les packages déjà migrés avec ce système et le numéro de version associé:

- `dtk-log           =3.0.0`
- `dtk-core          =3.0.0`
- `dtk-fonts         =3.0.0`
- `dtk-themes        =3.0.0`
- `dtk-script        =3.0.0`
- `dtk-distributed   =3.0.0`
- `dtk-widgets       =3.0.0`

En cours:
- `dtk-visualization =2.30.0`
- `dtkcore-python    =2.8`


## quelques trucs et astuces

- pb de `shallow cloning` sur mac-osx-arm64 pour dtk-distributed

  - résolu en mettant ces deux variables globales dans .gitlab-ci.yml

```
variables:
  GIT_STRATEGY: clone
  GIT_DEPTH: 0
```


## template de .gitlab-ci.yml

A remplacer en cas de refactoring...


```yml
#
# dtk group slaves:
#
# - osx      : dtk-macos-catalina.ci / sRZVDkCbN
# - osx-arm64: catalysis-zie         / j5KdEV2ss
# - linux    : dtk-centos-7.ci       / 3q8t4dgGP
# - windows  : dtk-win10.ci          / eNDyiy5uY
#
#

#
# global variables
#variables:

build-osx:
  stage: build
  needs: []
  tags:
    - osx
  only:
    - develop
    - merge_requests
  script:
    - source /Users/ci/Miniconda3/etc/profile.d/conda.sh
    - conda activate dtk-build
    - conda env remove -n $CI_PROJECT_NAME
    - mamba env update -f pkg/env/$CI_PROJECT_NAME.yaml
    - conda activate --stack $CI_PROJECT_NAME
    - rm -fr build
    - mkdir build
    - cd build
    - cmake ..
    - make -j`nproc`
    - make install
  artifacts:
    paths:
      - build/

build-osx-arm64:
  stage: build
  needs: []
  tags:
    - osx-arm64
  only:
    - develop
    - merge_requests
  script:
    - conda activate dtk-build
    - conda env remove -n $CI_PROJECT_NAME
    - mamba env update -f pkg/env/$CI_PROJECT_NAME.yaml
    - conda activate --stack $CI_PROJECT_NAME
    - rm -fr build
    - mkdir build
    - cd build
    - QT_HOST_PATH=${CONDA_PREFIX} cmake ..
    - make -j`nproc`
    - make install
  artifacts:
    paths:
      - build/

build-windows:
  stage: build
  needs: []
  tags:
    - windows
  only:
    - develop
    - merge_requests
  script:
    - . C:\Users\ci\Miniconda3\shell\condabin\conda-hook.ps1
    - conda activate dtk-build
    - conda env remove -n $CI_PROJECT_NAME
    - (Remove-Item "C:\Users\ci\Miniconda3\envs\$CI_PROJECT_NAME" -ErrorAction Ignore -Recurse)
    - mamba env update -f pkg/env/$CI_PROJECT_NAME.yaml
    - conda activate -stack $CI_PROJECT_NAME
    - (Remove-Item  "build" -ErrorAction Ignore -Recurse)
    - mkdir build
    - cd build
    - cmake ..
    - cmake --build . --config Release --target install
  artifacts:
    paths:
      - build/

build-linux:
  stage: build
  needs: []
  tags:
    - linux
  only:
    - develop
    - merge_requests
  script:
    - source /builds/miniconda3/etc/profile.d/conda.sh
    - conda activate dtk-build
    - conda env remove -n $CI_PROJECT_NAME
    - mamba env update -f pkg/env/$CI_PROJECT_NAME.yaml
    - conda activate --stack $CI_PROJECT_NAME
    - rm -fr build
    - mkdir build
    - cd build
    - cmake ..
    - make -j`nproc`
    - make install
  artifacts:
    paths:
      - build/

ctest-linux:
  stage: test
  needs: ["build-linux"]
  tags:
    - linux
  only:
    - develop
    - merge_requests
  script:
    - source /builds/miniconda3/etc/profile.d/conda.sh
    - conda activate dtk-build
    - conda activate --stack $CI_PROJECT_NAME
    - cd build
    - ctest -T test --timeout 120 --output-junit ../ctest-junit.xml
  artifacts:
    when: always
    reports:
      junit: ./ctest-junit.xml

ctest-osx-arm64:
  stage: test
  needs: ["build-osx-arm64"]
  tags:
    - osx-arm64
  only:
    - develop
    - merge_requests
  script:
    - conda activate dtk-build
    - conda activate --stack $CI_PROJECT_NAME
    - cd build
    - ctest -T test --timeout 120 --output-junit ../ctest-junit.xml
  artifacts:
    when: always
    reports:
      junit: ./ctest-junit.xml

ctest-osx:
  stage: test
  needs: ["build-osx"]
  tags:
    - osx
  only:
    - develop
    - merge_requests
  script:
    - source /Users/ci/Miniconda3/etc/profile.d/conda.sh
    - conda activate dtk-build
    - conda activate --stack $CI_PROJECT_NAME
    - cd build
    - ctest -T test --timeout 120 --output-junit ../ctest-junit.xml
  artifacts:
    when: always
    reports:
      junit: ./ctest-junit.xml

ctest-windows:
  stage: test
  needs: ["build-windows"]
  tags:
    - windows
  only:
    - develop
    - merge_requests
  script:
    - . C:\Users\ci\Miniconda3\shell\condabin\conda-hook.ps1
    - conda activate dtk-build
    - conda activate -stack $CI_PROJECT_NAME
    - cd build
    - ctest -T test --timeout 120 --output-junit ..\ctest-junit.xml
  artifacts:
    when: always
    reports:
      junit: ./ctest-junit.xml

build-conda-osx:
  stage: deploy
  needs: []
  tags:
    - osx
  only:
    - master
  script:
    - source /Users/ci/Miniconda3/etc/profile.d/conda.sh
    - conda activate base
    - conda mambabuild --token $DTK_FORGE_TOKEN --user dtk-forge -c dtk-forge -c conda-forge pkg/recipe/.

build-conda-osx-arm64:
  stage: deploy
  needs: []
  tags:
    - osx-arm64
  only:
    - master
  script:
    - conda activate base
    - conda mambabuild --token $DTK_FORGE_TOKEN --user dtk-forge -c dtk-forge -c conda-forge pkg/recipe/.

build-conda-windows:
  stage: deploy
  needs: []
  tags:
    - windows
  only:
    - master
  script:
    - . C:\Users\ci\Miniconda3\shell\condabin\conda-hook.ps1
    - conda activate base
    - conda mambabuild --token $DTK_FORGE_TOKEN --user dtk-forge -c dtk-forge -c conda-forge pkg/recipe/.

build-conda-linux:
  stage: deploy
  needs: []
  tags:
    - linux
  only:
    - master
  script:
    - source /builds/miniconda3/etc/profile.d/conda.sh
    - conda activate base
    - conda mambabuild --token $DTK_FORGE_TOKEN --user dtk-forge -c dtk-forge -c conda-forge pkg/recipe/.
```



## questions en attente

- liste complète des dtk-* à migrer/maintenir
- comment organiser la prochaine release ?

  - est-ce qu'on fait une release majeure (3.x.y) -> OUI
  - est-ce qu'on change le nommage (dtk3-*) -> NON
  - passage en revue de tous les yaml de `dtk-*` actuel (2.x.y) pour mettre des bornes supérieures aux dépendances ? -> OUI
  - est-ce qu'on passe aussi en revue les logiciels utilisant dtk-* (2.x.y), ex: diogenes, gnomon, windpos, macular, etc... -> au fur et à mesure

## questions ouvertes/améliorations

- merger les jobs build/test si on peut récupérer l'artifact de test à la fin

- EN COURS: scripter la recompilation de tous les modules `dtk-*` dans le bon ordre, en mode développement (le mode `release` est garantie par le release-master)

  - dans la branche develop
  - faire un script qui:
     - part d'un environnement conda vide et y installe juste cmake/qt6-main
     - boucle sur les `dtk-*` (dans le bon ordre) pour faire:

       - git clone
       - cmake / make / ctest /  make install

     - compile une appli finale de notre choix (une démo à la Ju :) )

- creation d'un compte dédié à dtk pour les compilations + env conda sur les slaves partagés (catalysis-zie)
- gitlab-runner sur showroom-[01]
