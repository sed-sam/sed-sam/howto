[Retour à l'index](./README.md)

Attention, zone avancée.
Vous voulez utiliser le debugger de Visual Studio avec votre projet C++. Belle curiosité !

![](https://media1.tenor.com/images/4cb9687bab51a3ef3daad8ee0ee3a378/tenor.gif?itemid=18514956)

# ATTENTION : ce tutoriel sera long et avec beaucoup d'étapes

## 1. Installer le Windows Subsystem for Linux, avec une distribution Linux

Si votre PC est déjà livré avec le "bash Ubuntu" vous pouvez sauter directement à la partie 1.2. **Je vous recommande fortement de suivre le point 1.2 et d'installer Debian car vous devrez la convertir au format WSL1 juste après, au point 1.3. Si vous avez déjà Ubuntu d'installé par je-ne-sais-qui, mieux vaut ne pas y toucher pour ce tuto !**



Personnellement je recommande Debian, il est léger, il y en a qu'un.

### 1.1. Activez la fonctionnalité WSL

- Dans le menu Démarrer, ouvrez les **Paramètres de Windows 10**, puis allez dans **Applications**.
- Cliquez sur **Fonctionnalités facultatives**, puis sur **Plus de fonctionnalités Windows**. Enfin, dans la petite fenêtre qui s'ouvre, cochez **Sous-système Windows pour Linux**, faites OK.

![](./images/wsl/activate-wsl.jpg)

- Redémarrez votre ordinateur

### 1.2. Installer Debian

- Ouvrez le Microsoft Store
- Cherchez "Debian"
- Cliquez sur Installer.
- Si le Microsoft Store vous demande de vous connecter avec un compte Microsoft, fermez la fenêtre. Vous n'en avez pas besoin et ça marchera tout aussi bien. (mais si vous êtes là c'est que vous avez déjà fait un compte Microsoft pour Visual Studio...)
![](./images/wsl/debian.jpg)


- Une fois l'installation terminée, ouvrez Debian. Renseignez un nom d'utilisateur et un mot de passe (ils peuvent être différents de ceux de votre laptop Windows!). Une fois que c'est fait, **fermez Debian**.


### 1.3. Convertir Debian au format WSL1

**<span style="color:red">WSL2 ne s'intègre pas avec le système de fichiers de Windows, ce qui rend la compilation par VS2019 impossible. Il faut convertir votre distribution en WSL1.</span>**

Ouvrez une Invite de Commandes Windows et entrez cette ligne :

```
wsl.exe --set-version Debian 1
```

**<span style="color:red">Toute la suite supposera que vous utilisez Debian en mode WSL1.</span>**

## 2. Démarrer votre projet C++

- ouvrez Debian
- installez les paquets essentiels au build: 

```bash
sudo apt update
sudo apt install build-essential git cmake uuid-dev libgl1-mesa-dev libxt-dev libxext-dev libsm-dev gdb wget
```
(note : le WSL n'intègre pas par défaut les libs pour la compilation d'applications graphiques, c'est pour ça qu'il faut les installer à la main) normalement ces deps couvrent la compilation de programmes Qt/C++. Si j'en ai oublié et qu'il en manque à la compilation, demandez aux Linuxiens du SED !)

- installez Miniconda3. Lors de l'installation laissez le répertoire par défaut. A la fin lorsqu'il demande s'il doit s'enregistrer dans le shell, faites **oui**.

```bash
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
chmod +x Miniconda3-latest-Linux-x86_64.sh
./Miniconda3-latest-Linux-x86_64.sh
```

- fermez et rouvrez un nouveau shell, vérifiez que conda est enregistré avec **(base)** devant votre bash.
- allez dans le répertoire de votre projet qui est stocké dans votre disque Windows C: : `cd "/mnt/c/Users/monNom/.../monprojet"`
- créez votre environnement conda pour Linux : `conda env create pkg/env/monprojet-linux.yaml`
- pour vérifier que tout marche : compilez et lancez la version Linux de votre projet avec la ligne de commande:

```bash
conda activate le_nom_de_mon_environnement
mkdir build-cli
cd build-cli
cmake ..
make -j3
```

- si tout est OK, fermez Debian.

Passons à Visual Studio 2019.

## 3. Configurer l'accès à WSL par VS2019

Dans le menu Démarrer, ouvrez **Visual Studio Installer**.

Pour **Visual Studio Community 2019** cliquez sur **Modifiez**.

Cochez **Développement Desktop C++ pour Linux** et cliquez sur **Modifier**.

![](./images/wsl/vsinstaller.jpg)

Editez avec votre éditeur de texte favori (ou directement avec Visual Studio 2019) le `CMakeSettings.json`. Ajoutez la nouvelle configuration suivante. <span style="color: red">Changez : </span>
- **le_nom_de_votre_environnement** avec le nom de votre environnement conda.
- **buildCommandArgs** : par défaut j'utilise 3 threads sur ma machine, vous pouvez adapter.

```json
    {
      "name": "WSL-GCC-Debug",
      "generator": "Unix Makefiles",
      "configurationType": "Debug",
      "buildRoot": "${projectDir}\\out-wsl\\build\\${name}",
      "installRoot": "$HOME/miniconda3/envs/le_nom_de_votre_environnement",
      "cmakeExecutable": "cmake",
      "cmakeCommandArgs": "-DCMAKE_PREFIX_PATH=$HOME/miniconda3/envs/le_nom_de_votre_environnement",
      "buildCommandArgs": "-j3",
      "ctestCommandArgs": "",
      "inheritEnvironments": [ "linux_x64" ],
      "wslPath": "${defaultWSLPath}",
      "addressSanitizerRuntimeFlags": "detect_leaks=0",
      "variables": []
    }
```
Ajoutez une nouvelle cible de lancement dans `launch.vs.json`. <span style="color: red">Contrairement au **CMakeSettings.json** tous les chemins ici doivent correspondre aux chemins du WSL, format Linux. Changez : </span>
- **name** : mettez un nom sympa.
- **ProjectTarget** : la cible à exécuter. Regardez les cibles disponibles dans le sélecteur de cibles (la liste déroulante du bouton Run) et prenez celle qui vous faut. Attention, les `\` doivent être doublés si le nom de votre cible en comporte (comme l'exemple ci-dessous).
- **cwd** : mettez le chemin de votre projet
- **args** : mettez les arguments que vous voulez, attention les chemins doivent être format WSL !
- **environment** : vous permet de spécifier des variables d'environnement, au format "name" / "value".

```json
    {
      "type": "cppgdb",
      "name": "Solver@WSL + ddp_dambreak",
      "project": "CMakeLists.txt",
      "projectTarget": "sw2dSolver (bin\\sw2dSolver)",
      "comment": "no comment",
      "cwd": "/mnt/c/Users/jolevy/Projets/sw2d/",
      "args": [ "-f", "/mnt/c/Users/jolevy/Projets/sw2d/examples/ddp_dambreak" ],
      "externalConsole": true,
      "environment": [
        {
          "name": "MA_VARIABLE_DENVIRONNEMENT",
          "value": "mavaleurdevariable"
        }
      ]
    }
```

Chargez la configuration CMake **WSL-GCC-Debug** dans le sélecteur de configuration CMake.

![](./images/wsl/vs2019-config-selector.jpg)

Puis dans la barre de menus, faites **Projet -> Générer le cache**.

A cet instant, pour la première génération pour votre projet, VS2019 va vous afficher un bandeau jaune demandant d'installer sa propre version de CMake pour lui. Dans le bandeau, cliquez **oui**.

![](./images/wsl/vs2019-cmake-ribbon.jpg)

Après avoir installé la version de CMake pour le projet  il va continuer la génération du cache CMake.

Mettez des breakpoints en cliquant à gauche des lignes, ça fait des petits cercles rouges. Vous pouvez même les customiser avec un clic droit (par exemple les faire marcher si une condition est validée...)

![](./images/wsl/vs2019-breakpoints.jpg)

Lancez votre target custom : vous pouvez vous arrêter à des breakpoints et explorer l'état de votre programme.


## 4. Debugger un programme graphique

OK là on part dans les meilleurs bails.

### 4.1. Installer VcxSrv, le serveur X 

- [Téléchargez le serveur X VcxSrv ici.](https://sourceforge.net/projects/vcxsrv/files/latest/download). Il va servir à afficher les programmes graphiques lancés par WSL.
- Installez le.
- Lancez le en exécutant **XLaunch** depuis votre menu démarrer.
- Quand vous le lancez, <span style="color: red">spécifiez systématiqueemnt ces paramètres :</span>
  * **Display number : 0**
  * Start no client
  * **Native OpenGL : décochée**
  * **Disable access control : cochée**

![](./images/wsl/vcxsrv.jpg)

Le serveur X est maintenant lancé.

Ouvrez Debian et copiez-collez la ligne suivante. Elle se chargera d'informer le bash qu'il peut utiliser votre serveur X.

```bash
echo "export DISPLAY=localhost:0.0" >> ~/.bashrc
```

### 4.2. Configurer une cible de lancement pour Visual Studio 2019

A présent vous pouvez faire une cible de lancement dans **launch.vs.json** avec comme **projectTarget** la cible contenant le programme graphique, et avec une variable d'environnement `DISPLAY`, à laquelle vous assignez la valeur `localhost:0.0`.

```bash
    {
      "type": "cppgdb",
      "name": "Modeler@WSL",
      "project": "CMakeLists.txt",
      "projectTarget": "sw2dModeler (bin\\sw2dModeler)",
      "comment": "no comment",
      "cwd": "/mnt/c/Users/jolevy/Projets/sw2d/",
      "args": [ ],
      "externalConsole": true,
      "environment": [
        {
          "name": "DISPLAY",
          "value": "localhost:0.0"
        }
      ]
    }
```

Lancez votre nouvelle cible, et votre programme graphique doit apparaitre (sous vos yeux ébahis!). Vous pouvez placer des breakpoints, voir l'état des variables, et faire tout plein de trucs.

<img src="https://media1.tenor.com/images/a65689f303058581fe2ae4e6e8ba5bc1/tenor.gif?itemid=20172957" height="300px"/>

*C'est cool hein ? Bon c'est terriblement tordu mais c'est cool hein ?*

## Remarques

L'éditeur de code de VS2019 a du mal à trouver les dépendances du projet quand il est en mode **WSL-GCC-Debug**. Il va vous afficher plein d'erreurs dans le code à cause de ça. Repassez en mode **Release x64** (compilation native Windows) pour qu'il détecte à nouveau vos dépendances et qu'il puisse vous aider à écrire votre code. Le mode **WSL-GCC-Debug** est là quand vous avez vraiment besoin de debugger.

