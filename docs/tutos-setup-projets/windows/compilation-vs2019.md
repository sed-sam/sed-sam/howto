[Retour à l'index](./README.md)

Prérequis :
- [Installer Git et récupérer son repo](./install-git.md)
- [Installer Miniconda3 et récupérer son environnement](./install-miniconda3.md)

Il est temps de compiler le projet !

![](https://media1.tenor.com/images/5f08ccadfab581517f073245adb5f68c/tenor.gif?itemid=8087657)

## 1. Installer Visual Studio 2019 avec la compilation C++

<span style="color:red">Note : Visual Studio 2019 est la dernière version en date au moment de l'écriture. Si une version plus récente est sortie, vous pouvez l'utiliser, à condition d'ajuster les fichiers de configuration. Je vous conseille d'ailleurs de le faire, vous comprendrez comment ça marche, c'est beaucoup plus formateur !</span>

Windows a besoin des outils de compilations spécifiques pour compiler des programmes. Ils sont livrés avec Visual Studio 2019, qui est un environnement de développement très complet (il permet d'écrire le code, de compiler, d'exécuter, et de gérer vos modifications Git de façon agréable.)

[Téléchargez Visual Studio 2019 en cliquant ici.](https://visualstudio.microsoft.com/fr/thank-you-downloading-visual-studio/?sku=Community&rel=16) Cela téléchargera le gestionnaire d'installation **Visual Studio Installer**. Une fois qu'il sera installé, vous serez accueilli par la fenêtre de sélection de composants. Dans le paramétrage, cochez **Développement Desktop en C++**.

![](./images/compilation-vs2019/vs2019-desktop-cpp.jpg)


L'installation est longue, armez-vous de patience.

## 2. Paramétrer Visual Studio 2019

<p style="color: red; font-size: 1.4em; front-weight: 701">Préambule : à cause de l'utilisation de <strong>conda</strong> pour gérer les dépendances de projet, il n'est pas possible d'utiliser les fonctions de debugging sous Windows. Pour cela, toutes les compilations doivent se faire en mode "Release". Si vous souhaitez absolument utiliser les fonctions de debugging, c'est possible de le faire en compilant la version Linux de votre projet avec WSL. Quand vous aurez fini les autres tutos, reportez-vous au tutoriel <strong>pour utilisateurs avancés<strong> à ce sujet (mais seulement après avoir terminé les autres tutoriels Windows.)</p>

Si votre projet contient déjà ce fichier :

- **CMakeSettings.json**

C'est qu'un précédent utilisateur l'a déjà configuré pour vous. Vous n'avez rien à faire de plus ! Passez à l'étape suivante.

Si ce fichier n'est pas présent dans le répertoire de votre projet, ouvrez votre éditeur de texte préféré et créez-le dans le dossier de votre projet.

**CMakeSettings.json** va servir à Visual Studio à trouver votre environnement conda pour qu'il puisse compiler avec les dépendances du projet. Regardez [le CMakeSettings.json d'exemple](./exemples/CMakeSettings.json.md) et copiez-collez son contenu. 

Deux informations à mettre à jour dans ce fichier :
1. <span style="color: red">Changez **LE_NOM_DE_MON_ENVIRONNEMENT** par le nom de votre environnement conda. Si vous ne savez pas comment il s'appelle, regardez son nom dans le fichier `pkg\envs\monprojet-windows.yaml`.</span>.
2. Si l'installation de l'environnement n'est pas dans `%USERPROFILE%\Miniconda3", il faut remplacer toutes les occurences de `%USERPROFILE%\Miniconda3" par l'emplacement réel.

Plus tard si le SED vous parle de "variables d'environnement de CMake" c'est dans ce fichier qu'il faudra venir rajouter des variables.

## 3. Ouvrir le projet et compiler

Ouvrez Visual Studio 2019. Au premier lancement il vous propose de choisir le thème de couleur, moi je préfère sombre. 

Si vous n'utilisez pas de compte Microsoft sur votre PC, il va falloir en créer un pour utiliser Visual Studio. C'est un compte mail avec un adresse qui termine par **outlook.com, outlook.fr, hotmail.com**. [Vous pouvez créer un compte ici.](https://outlook.live.com/owa/).

Quand Visual Studio demandera de vous connecter, connectez-vous avec votre compte.

Ouvrez le dossier de votre projet. Générez le cache de CMake en cliquant sur **Projet -> Générer le cache (1) puis (2).**

![](./images/compilation-vs2019/vs2019-cmake-cache.jpg)

Vous devrez observer que le cache se génère sans erreur dans la console dans la partie basse de la fenêtre.

Sélectionnez une cible de build. **Il n'y a que les cibles (Installer) qui marchent**. Par exemple, avec sw2dModeler, je peux sélectionner **sw2dModeler.exe (Installer)**.

![](./images/compilation-vs2019/vs2019-build.jpg)

Ca doit compiler et à la fin ça doit exécuter votre programme !

![](https://media1.tenor.com/images/d74964da2f1728dd6a0afb84800972a1/tenor.gif?itemid=16168168)

Pour lancer un programme avec un paramètre en ligne de commande, lisez la partie suivante.

## 4. Configurer des cibles de lancement

**launch.vs.json** va servir à définir des commandes à lancer après la compilation. Il est possible que ce fichier existe déjà si quelqu'un l'a déjà créé pour vous. Sinon, pour l'écrire, vous pouvez regarder [le launch.vs.json d'exemple](./exemples/launch.vs.json.md) et copiez-collez son contenu. <span style="color: red">Changez :</span>
- **projectTarget** par la cible de votre projet, **elle doit contenir (Installer)**
- **name** par un petit nom sympa,
- **args** en mettant à la suite les arguments que vous voulez rentrer pour votre programme.

![](./images/compilation-vs2019/vs2019-launch-targets.jpg)

Une fois que c'est fait, vous devriez voir votre nouvelle cible dans le menu des cibles de lancement. Cliquez, ça compile, ça lance. 

C'est pas mal, non ?

![](https://media1.tenor.com/images/142aa009cb9f444deac5310548e677f5/tenor.gif?itemid=16381908)

Y'a plus qu'à coder.

![](https://media1.tenor.com/images/169155eb18f274d2f7793b0029642908/tenor.gif?itemid=17687730)

Lire la suite : [Utiliser Git avec Visual Studio 2019](./git-vs2019.md)
