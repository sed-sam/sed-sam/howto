[Retour à l'index](./README.md)

Prérequis :
- [Installer Git et récupérer son repo](./install-git.md)

Au SED on utilise **conda**, un outil qui permet de récupérer d'un coup tous les paquets nécessaires à un projet.

**conda** est disponible dans plusieurs versions, une grosse qui s'appelle **Anaconda** et une petite qui s'appelle **Miniconda**. On utilise toujours la petite, **Miniconda**.

## 1. Télécharger Miniconda3

[Cliquez ici pour télécharger Miniconda3.](https://repo.anaconda.com/miniconda/Miniconda3-latest-Windows-x86_64.exe)

Lancer l'installateur, cliquez sur Suivant autant de fois que nécessaire, puis Terminer.

Ainsi qu'indiqué par l'installateur, **il ne faut pas avoir d'espace dans le chemin d'accès** au répertoire d'installation à miniconda (Y'en a qu'ont essayé, ils ont eu des problèmes).

## 2. Créer l'environnement conda propre à mon projet

Chaque projet au SED possède un ensemble de paquets, propre à Windows, ou macOS, ou Linux, qu'on appelle **environnement conda**. Par convention au SED on place l'information de cet environnement dans le fichier `pkg\env\monprojet-windows.yaml` (ou autre OS, etc.)

Dans votre menu démarrer, il y a un nouveau raccourci qui s'appelle **Anaconda Prompt (Miniconda3)**. Ouvrez-le.

Dans la fenêtre qui s'ouvre, vous avez une ligne de commande Windows. Avec la commande `cd NomDunDossier`, allez dans le dossier où vous avez cloné votre projet. Pour revenir en arrière, utilisez `cd ..`.

Par exemple, si mon projet se trouve dans `C:\Users\Jonathan\Projets\sw2d` alors je peux taper :

```
(base) C:\Users\Jonathan>cd Projets
(base) C:\Users\Jonathan\Projets>cd sw2d
(base) C:\Users\Jonathan\Projets\sw2d>
```

Je peux aussi taper en une seule fois:
```
(base) C:\Users\Jonathan>cd C:\Users\Jonathan\Projets\sw2d
(base) C:\Users\Jonathan\Projets\sw2d>
```

Là, pour créer mon environnement conda, il faut taper:

```bat
conda env update -f pkg\env\monprojet-windows.yaml
```
La commande `update` crée l'environnement lorsqu'il n'existe pas encore, et met à jour les paquets sinon.

**conda** va venir lire le fichier `monprojet-windows.yaml` qui doit se trouver dans le répertoire `pkg\env`. Cela va télécharger et installer tous les paquets dont vous avez besoin. A priori, vous n'aurez plus à y retoucher.

## 3. Mettre à jour un environnement conda

Si une mise à jour ne suffit pas et qu'il est nécessaire de re-créer intégralement l'environnement conda, vous pouvez le faire avec les commandes suivantes :

```
conda env list
conda env remove -n le_nom_de_mon_environnement
conda clean --all
conda clean --force-pkgs-dir
conda env update -f pkg\env\monprojet-windows.yaml
```

## 4. Quelques commandes utiles de conda

- `conda activate le_nom_de_mon_environnement` : vous permet d'utiliser les binaires stockés dans votre environnement (voir plus tard)
- `conda deactivate` : vous permet de désactiver un environnement précédemment activé
- `conda env list` : vous permet de vos vos environnement et savoir lequel est activé
- `conda list` : vous permet de savoir quels sont les paquets conda installés dans votre environnement.

Bon! On a notre projet, on a notre environnement conda associé, il ne reste plus qu'à paramétrer la compilation et compiler ! 

![](https://media1.tenor.com/images/28230f49f74b133f164aba4e0658715f/tenor.gif?itemid=19348685)

## Lire la suite : [Compiler avec Visual Studio 2019](./compilation-vs2019.md)
