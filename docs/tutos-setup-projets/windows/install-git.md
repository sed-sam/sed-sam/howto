[Retour à l'index](./README.md)

**Git** est un logiciel qui permet de travailler en équipe sur du code, de marquer sa progression étape par étape et de rassembler la progression de chacun.

## 1. Installer Git (vous allez voir c'est facile)

[Cliquez ici pour télécharger l'installateur de Git.](https://git-scm.com/download/win)

Téléchargez l'installateur, puis ouvrez-le. Cliquez sur "Suivant".
Quand l'installateur vous demande quel éditeur de texte utiliser, choisissez votre éditeur préféré. Si vous n'en savez rien, sélectionnez Notepad. Laissez toutes les autres options par défaut.

![](./images/install-git/git-editor.jpg)

## 2. Utiliser une clé SSH pour se connecter à Gitlab

Une **clé SSH** est un ensemble de 2 fichiers qui vous permettent de vous connecter à différents services en ligne (je simplifie un peu). 

- la "clé publique" est comme **la serrure d'une porte** : chacun a la sienne, mais tout le monde dans la rue peut la voir (ça n'est problème)
- la "clé privée" est comme **la clé correspondant à la serrure** : elle ne marche qu'avec *votre* serrure, et ne doit surtout pas être divulguée dans la nature (ou sur internet).

Si vous n'en avez pas, il faut générer votre propre paire clé publique/clé privée.

Vous devez à présent récupérer votre repository git. Ouvrez l'explorateur de fichiers, allez là où vous voulez mettre votre projet (par exemple Mes Documents), faites un clic droit, puis sélectionnez "Git bash here".

![](./images/install-git/git-right-click.jpg)

Dans la fenêtre qui s'ouvre, tapez la commande : 

```
ssh-keygen -t ed25519 -C "votreadresseemail@inria.fr"
```
Quand il vous demande 
```
Generating public/private ed25519 key pair.
Enter file in which to save the key (/c/Users/Jonathan/.ssh/id_ed25519):
```
Appuyez sur Entrée.
Ensuite vous devrez rentrer un mot de passe associé à votre clé (SSH appelle ça **une passphrase**). Vous pouvez ne rien rentrer, mais c'est très déconseillé (si vous faites une erreur, que votre fichier "clé privée" se retrouve sur internet et que quelqu'un le trouve, ils pourront accéder à vos données protégées !). Rien n'est affiché pendant que vous tapez, c'est normal, mais le clavier marche, promis.

```
Enter passphrase (empty for no passphrase): TAPEZ_UN_MOT_DE_PASSE
Enter same passphrase again: TAPEZ_LE_MEME_MOT_DE_PASSE
Your identification has been saved in /c/Users/Jonathan/.ssh/id_ed25519
Your public key has been saved in /c/Users/Jonathan/.ssh/id_ed25519.pub
The key fingerprint is:
SHA256:vne0kaOLR/83CYSP1494SoSOGNy9qDZC8DqOCIrJ8R0 jonathan.levy@inria.fr
The key's randomart image is:
+--[ED25519 256]--+
|                 |
|                 |
|            .    |
|  .  . . . o .   |
|   o  o S o =..  |
|    o  + +.+=+ . |
|.. o E. +.o+o=..o|
|*o= o +. o+.= ooo|
|*o.o +..oo.o.+...|
+----[SHA256]-----+
```

Ouvrez ensuite votre explorateur Windows, allez dans `C:\ --> Utilisateurs --> Votre Nom --> .ssh`. Vous devrez voir 2 fichiers `id_ed25519.pub` et `id_ed25519`. Ouvrez le fichier `id_ed25519.pub` avec votre éditeur de texte préféré (ou le Bloc-Notes).

Rendez-vous dans [vos paramètres de Gitlab en cliquant ici](https://gitlab.inria.fr/-/profile/keys). Copiez-collez le contenu du fichier `id_ed25519.pub` dans le champ de texte, puis cliquez sur **Add key**. Notez bien : je peux divulguer ma clé publique, je m'en fiche, elle est *publique* (c'est la "serrure", vous pouvez bien voir toutes les serrures de toutes les portes dans la rue !)

![](./images/install-git/gitlab-key.jpg)

Vous pouvez maintenant travailler avec Gitlab !



## 3. Récupérer votre *repository* Git

Sur la page de votre projet Gitlab, trouvez la ligne de clonage avec le bouton "clone". Copiez la ligne "Clone with SSH".

![](./images/install-git/gitlab-clone.jpg)

Dans le Git Bash que vous avez toujours ouvert, vous pouvez maintenant entrer la commande :

```
git clone [INSEREZ ICI LA LIGNE DE CLONAGE DE GITLAB]
```

Après vous avoir demandé la passphrase associée à votre clé SSH, le répertoire sera copié et les informations de git enregistrées.

Pour finir, il faut configurer votre identité dans votre répertoire. Après le clonage, entrez les commandes suivantes:

```
cd LeNomDeVotreProjet
git config user.name "MonPrénom MonNomDeFamille"
git config user.email "monemailinria@inria.fr"
```

Vous avez terminé avec git bash ! Vous pouvez le fermer (à tout jamais).

![](https://media1.tenor.com/images/fb9249a9a75b2e0129b496438d7c7ad8/tenor.gif?itemid=9517014)

## Lire la suite : [Installer Miniconda3 et récupérer son environnement](./install-miniconda3.md)
