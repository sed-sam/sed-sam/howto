[Retour à l'index](./README.md)

Par curiosité, ou peut-être parce que vous avez vos habitudes avec Git et votre éditeur de texte préféré, vous voudriez peut-être compiler manuellement avec la ligne de commande. *Attention : sans Visual Studio 2019 vous ne profitez pas de la gestion de Git graphique. Faites attention à vos opérations avec Git !*

Dans le dossier de votre projet, créez un fichier, nommez-le par exemple **compilation.bat** et copiez-collez le contenu suivant. <span style="color: red">Changez **le_nom_de_mon_environnement** avec le nom de votre environnement conda.</span>

```bat
@echo off
mkdir build-bat
cd build-bat
set CONDA_PREFIX="%USERPROFILE%\Miniconda3\envs\le_nom_de_mon_environnement"
set BUILD_CONFIG=Release
set LIBRARY_LIB=%CONDA_PREFIX%\Library\lib
set LIBRARY_PREFIX=%CONDA_PREFIX%\Library

cmake .. -G "Visual Studio 16 2019" ^
-Wno-dev ^
-DCMAKE_INSTALL_PREFIX=%LIBRARY_PREFIX% ^
-DCMAKE_PREFIX_PATH=%LIBRARY_PREFIX% ^
-DCMAKE_INSTALL_RPATH:STRING=%LIBRARY_LIB% ^
-DCMAKE_INSTALL_NAME_DIR=%LIBRARY_LIB%

if errorlevel 1 echo "Can't generate CMake config"
cmake --build . --parallel %NUMBER_OF_PROCESSORS% --config %BUILD_CONFIG% --target install
cd ..
if errorlevel 1 echo "Can't build generated CMake config"
```

- Dans le menu démarrer, cherchez *Developer Command Prompt for VS 2019* et ouvrez-le.
- Dans ce Developer Command Prompt, allez dans le dossier de votre projet avec la commande `cd`. 
- Lancez le script de compilation en tapant simplement son nom.

```
compilation.bat
```

- Le script de compilation cible la *target __install__*. Les binaires compilés sont accessibles dans l'environnement conda associé.
- Dans le menu démarrer, cherchez *Anaconda Prompt (Miniconda3)* et ouvrez-le.
- Dans cet Anaconda Prompt, activez votre environnement conda avec la commande:

```
conda activate le_nom_de_mon_environnement
```

- Vous pouvez lancer votre exécutable en tapant son nom, et avec les arguments que vous souhaitez:

```
monBinaire.exe -f argument1 -p argument2...
```