[Retour à l'index](./README.md)

Prérequis :
- [Installer Git et récupérer son repo](./install-git.md)
- [Installer Miniconda3 et récupérer son environnement](./install-miniconda3.md)
- [Compiler avec Visual Studio 2019](./compilation-vs2019.md)


**Git**, pour un débutant, c'est un peu un dragon. 

![](https://media.giphy.com/media/3oKIPyy9jfV3RWyxCo/giphy.gif)

C'est un logiciel complexe, qui s'utilise traditionnellement en ligne de commande, qui a *énormément* de commandes et dont le fonctionnement est un peu abstrait. 

![](https://media.giphy.com/media/Ba5wxXM0ocEYU/giphy.gif)

*Deux développeurs qui veulent que vous utilisiez Git en ligne de commande ; vue d'artiste.*


Commencer Git et comprendre ce qu'il fait en partant de la ligne de commande, c'est *ultra galère*. Heureusement, Visual Studio 2019 intègre tout ce qu'il faut pour qu'on dompte le dragon, et surtout, qu'on comprenne ce qui se passe.

## 1. Comprendre le principe

### 1.1. Des commits sur une branche

Git a deux buts :
- marquer sa progression quand on code,
- rassembler des morceaux de code qui ont été écrits séparément (par des personnes différentes).

Marquer sa progression, on fait ça avec des **commits**. Un commit se fait en 3 temps :
- on ajoute à Git les fichiers qui n'existaient pas avant
- on sélectionne les modifications qu'on veut enregistrer
- on rentre un petit message pour indiquer à quoi correspond ce qu'on a fait.

Chaque commit se fait dans une **branche**. Une branche, c'est simplement une succession de commits. 

Le dossier de votre projet possède une copie sur les serveurs d'Inria avec laquelle vous intéragissez à travers le site gitlab.inria.fr. C'est même là que vous avez trouvé l'information pour cloner ce dossier en SSH !

Vous pouvez synchroniser l'état de votre dossier (sur votre ordinateur) avec l'état du serveur (que tout le monde voit et partage sur gitlab.inria.fr) à l'aide de 2 opérations simples :

- **git pull**: récupérer l'état du serveur vers votre ordinateur,
- **git push**: envoyer l'état de votre projet sur votre ordinateur vers le serveur.

C'est pour ça qu'on fait des *branches* : si tout le monde commençait à modifier des trucs en même temps, impossible de savoir quoi garder ! Une branche vous permet donc de travailler **de façon indépendante des autres**, jusqu'au moment (fatidique) où vous devrez *fusionner* l'état de votre branche de travail avec l'état *général* du programme. C'est l'étape de *fusion*, mais en général on dit **merge** (en anglais).

### 1.2. La manoeuvre classique

Pour résumer :
- tout le monde part d'une branche commune, chez nous elle s'appelle **develop**
- quand on veut réparer quelque chose ou ajouter une fonction, on *crée une branche*,
- on progresse dans sa branche en faisant des *commits*,
- on envoie régulièrement ses commits sur le serveur avec l'opération de **git push**
- on demande de *fusionner* ses changements dans la branche commune **develop** (c'est la *merge request* et c'est en dehors de ce tuto là.)
- quand la fusion dans **develop** est faite, on pense à synchroniser la copie de la branche **develop** de son ordinateur en récupérant toutes les nouvelles modifications avec l'opération **git pull**.

Voilà pour le principe ! Et d'habitude tout le monde tape des commandes pour faire tout ça. Mais voyons comment on fait ça avec Visual Studio.

![](https://media1.tenor.com/images/2f880ddd7348e9f0007bc5af1f468ece/tenor.gif?itemid=13119038)

*Désolée les gars, moi j'veux des boutons à cliquer et j'veux voir ce qui se passe dans votre machin git là*

## 2. Utiliser Git avec Visual Studio 2019

Visual Studio 2019 a son propre menu pour Git, en haut de la fenêtre. 

![](./images/git-vs2019/menu-git.jpg)

Vous reconnaissez déjà les noms des opérations : 
- Tirer (**pull**)
- Envoyer (**push**)
- **Commit** ou stash... 
- Nouvelle **branche**
- Voir l'historique de la **branche**
- Gérer les **branches**


il y a aussi Récupérer (**fetch**) (mais on n'en parlera pas, vous chercherez la différence entre "pull" et "fetch" si ça vous intéresse)...

En cliquant sur **Gérer les branches** vous aurez une vue graphique où chaque point est un commit. **En double-cliquant sur un commit**, vous verrez les fichiers modifiés sur la droite, puis en double-cliquant sur le fichier, vous aurez une vue avec les différences avant/après.

![](./images/git-vs2019/git-commit-diff.jpg)

Bon il est temps de faire ses propres branches et commits.

## 3. Faire ses branches et ses commits

En bas à droite est affichée **la branche active**. Vous pouvez changer de branche en cliquant dessus. Pour le moment, dans le menu Git, sélectionnez *Nouvelle branche...*, et créez une nouvelle branche à partir de **develop**. Après l'avoir créé, VS2019 va passer sur la nouvelle branche.

<p style="color: red; font-size: 1.4em; front-weight: 701">Par convention, toutes les branches où on développe des nouvelles fonctionnalités doivent être basées sur <strong>develop</strong> et commencer par <strong>feature/</strong>.</p>

![](./images/git-vs2019/git-new-branch.jpg)

Dans le menu Git, sélectionner "Commit ou stash...". La vue des modifications s'ouvre. Vous devez alors sélectionner les changements que vous voulez valider avec le bouton + (1), puis écrire un message sur vos modifications (2), et enfin valider le commit (3). Quand votre commit est fait, vous pouvez **l'envoyer** (push) avec le petit bouton en forme de flèche (4).

![](./images/git-vs2019/git-commit.jpg)

## 4. Faire une *fusion* (**merge**)

La branche **develop** a connu de nouveaux changements. Il faut alors :
- récupérer les changements de develop:
    * passer sur la branche **develop** à l'aide du sélecteur des branches en bas à droite.
    * récupérer les nouveautés: dans le menu Git --> **Tirer (pull)**
- revenir sur votre branche **feature/...** (1)
- dans le menu Git --> **Gérer les branches**
- Clic droit sur la branche **develop (2)**, puis **fusionner 'develop' dans la branche 'feature/...' (3)**

![](./images/git-vs2019/git-merge.jpg)

S'il y a des modifications de **develop** qui sont en conflit avec celles de votre branche, il va falloir déterminer **quelles modifications garder**. Et pour cela... il va falloir résoudre le conflit dans un **nouveau commit que Git vous a préparé**. En double-cliquant sur les fichiers incriminés (à droite entouré en rouge) vous verrez où est le problème.

![](./images/git-vs2019/git-merge-conflict0.jpg)

Pour résoudre le conflit, on procède comme ceci (voir l'image ci-après):
- pour chaque fichier, on regarde le conflit, et suivant ce qu'il y a, on décide de garder l'un ou l'autre (ou les deux). **(1)** Tout dépend de ce qui est écrit, c'est à vous de décider !
- on regarde le résultat dans la fenêtre en bas **(2)**
- on sauvegarde la modification faite **(3)**
- on ajoute le fichier à la liste des fichiers à commit: clic droit, **Indexer (4)**.
- quand on a résolu tous les conflits pour tous les fichiers, et qu'on les a tous indexés, on écrit un message à propos du *merge* **(5)**
- enfin, on valide le commit **(6)** (le bouton devient accessible quand tous les conflits ont été résolus).
- pensez à **Envoyer (push)** votre branche après avoir fait le merge ! Vous venez de faire un commit, après tout.

![](./images/git-vs2019/git-merge-conflict1.jpg)

Et voilà, vous savez faire le truc le plus chiant de Git. Visual Studio aide beaucoup pour choisir ce qu'on veut garder, imaginez s'il faut faire tout ça en tapant des commandes et en supprimant des morceaux de fichier texte... là, Visual Studio vous guide pour résoudre les problèmes.

## 4. Aide-mémoire

- Penser à **pull** la branche **develop** avant de créer une nouvelle branche.
- Penser à **push** votre branche régulièrement (souvent on le fait à chaque commit, c'est très bien)
- Penser à **merge develop** dans votre branche avant de faire une merge request sur Gitlab

## Conclusion

Vous êtes venus avec un dragon, vous repartez avec... une bonne à tout faire. 

Somme toute :

*Git est balèze.*

![](https://media1.tenor.com/images/942e2819a1c9206b60acf778e5467d75/tenor.gif?itemid=14283772)

*Git est sympa.*

![](https://media1.tenor.com/images/8b00c464465b4ad9ead8db11ccdbdba2/tenor.gif?itemid=9905373)

*Git est votre ami.*

![](https://media.giphy.com/media/3oKIPEfH3cqyDQYxag/giphy.gif)

