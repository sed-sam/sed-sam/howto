[Retour à l'index](../README.md)

Dans l'ordre:

## [Installer Git et récupérer son repo](./install-git.md)

## [Installer Miniconda3 et récupérer son environnement](./install-miniconda3.md)

## [Compiler mon projet C++ avec Visual Studio 2019](./compilation-vs2019.md)

## [Utiliser Git avec Visual Studio 2019](./git-vs2019.md)

## [BONUS : compiler mon projet C++ avec un script de build](./compilation-windows-cli.md)

______________________

Attention : tutoriel avancé.

## [Utiliser le debugger graphique de Visual Studio 2019 pour mon projet C++ avec le WSL](./advanced-debugging-vs2019-wsl.md)
