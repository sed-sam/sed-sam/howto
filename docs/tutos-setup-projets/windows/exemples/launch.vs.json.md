## Exemple : **launch.vs.json**

```json
{
  "version": "0.2.1",
  "defaults": {},
  "configurations": [
    {
      "type": "native",
      "name": "Solver + dip_sacramento",
      "project": "CMakeLists.txt",
      "projectTarget": "sw2dSolver.exe (Installer)",
      "comment": "no comment",
      "noDebug": true,
      "cwd": "${projectDir}",
      "args": [ "-f", "${projectDir}\\examples\\dip_sacramento" ],
      "env": {}
    },
    {
      "type": "native",
      "name": "Solver + ddp_dambreak",
      "project": "CMakeLists.txt",
      "projectTarget": "sw2dSolver.exe (Installer)",
      "comment": "no comment",
      "noDebug": true,
      "cwd": "${projectDir}",
      "args": [ "-f", "${projectDir}\\examples\\ddp_dambreak" ],
      "env": {}
    }
  ]
}
```