## Exemple : **CMakeSettings.json**

```json
{
    "configurations": [
        {
            "name": "x64-Release",
            "generator": "Visual Studio 16 2019 Win64",
            "configurationType": "Release",
            "buildRoot": "${projectDir}/out/build/${name}",
            "installRoot": "%USERPROFILE%/Miniconda3/envs/LE_NOM_DE_MON_ENVIRONNEMENT/Library/",
            "cmakeCommandArgs": "",
            "buildCommandArgs": "",
            "ctestCommandArgs": "",
            "inheritEnvironments": [
                "msvc_x64_x64"
            ],
            "intelliSenseMode": "windows-msvc-x64",
            "variables": [
                {
                    "name": "CMAKE_PREFIX_PATH",
                    "value": "%USERPROFILE%/Miniconda3/envs/LE_NOM_DE_MON_ENVIRONNEMENT/Library/",
                    "type": "STRING"
                },
                {
                    "name": "CMAKE_INSTALL_RPATH",
                    "value": "%USERPROFILE%/Miniconda3/envs/LE_NOM_DE_MON_ENVIRONNEMENT/Library/lib/",
                    "type": "STRING"
                },
                {
                    "name": "CMAKE_INSTALL_NAME_DIR",
                    "value": "%USERPROFILE%/Miniconda3/envs/LE_NOM_DE_MON_ENVIRONNEMENT/Library/lib/",
                    "type": "STRING"
                }
            ]
        }
    ]
}
```