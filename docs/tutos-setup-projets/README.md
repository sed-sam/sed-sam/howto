[Retour à l'index](../README.md)

Voici quelques tutos à destination des nouveaux venus au SED et aux équipes de chercheurs (non-SED, non-développeurs aguerris) pour accompagner dans la prise en main des outils quotidiens de développement.

## [Pour Windows](./windows/README.md)

## [Pour macOS - ⚠ à écrire ⚠](./macos/README.md)

## [Pour Linux - ⚠ à écrire ⚠ ](./linux/README.md)
