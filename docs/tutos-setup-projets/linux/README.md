[Retour à l'index](../README.md)

## [Installer les outils de développements macOS](./dev-linux.md)

## [Installer Git et récupérer son repo](./install-git.md)

## [Installer Miniconda3 et récupérer son environnement](./install-miniconda3.md)

## [Compiler mon projet C++ avec CMake /!\ A ECRIRE /!\](./compilation-cmake.md)