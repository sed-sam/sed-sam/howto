[Retour à l'index](../README.md)

Vous avez besoin des outils de développement en ligne de commande de Linux pour compiler des projets C++.

Pour une distrubution Debian/Ubuntu, installez ces outils avec cette commande :

```bash
sudo apt update
sudo apt install build-essential git cmake uuid-dev libgl1-mesa-dev libxt-dev libxext-dev libsm-dev gdb wget
```

Pour Fedora ça doit être un truc comme ça. Je ne suis pas tout à fait sûr, en cas de doute demandez à un ingénieur du SED ! 

```bash
sudo dnf update
sudo dnf groupinstall "Development Tools"
sudo dnf install git cmake mesa-libGL-devel libuuid-devel libXt-devel libXext-devel libSM-devel gdb wget
```
