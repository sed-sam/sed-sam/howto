[Retour à l'index](./README.md)

Prérequis :
- [Installer Git et récupérer son repo](./install-git.md)

Au SED on utilise **conda**, un outil qui permet de récupérer d'un coup tous les paquets nécessaires à un projet.

**conda** est disponible dans plusieurs versions, une grosse qui s'appelle **Anaconda** et une petite qui s'appelle **Miniconda**. On utilise toujours la petite, **Miniconda**.

## 1. Télécharger Miniconda3

- installez Miniconda3. Lors de l'installation laissez le répertoire par défaut. A la fin lorsqu'il demande s'il doit s'enregistrer dans le shell, faites **oui**.

```bash
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
chmod +x Miniconda3-latest-Linux-x86_64.sh
./Miniconda3-latest-Linux-x86_64.sh
```

## 2. Créer l'environnement conda propre à mon projet

Ouvrez un terminal. Pour vous déplacer de dossier en dossier, utilisez la commande `cd monDossier`. Vous pouvez revenir au dossier parent avec `cd ..`

Là, pour créer mon environnement conda, il faut taper:

```bash
conda env create -f pkg/env/monprojet-linux.yaml
```

**conda** va venir lire le fichier `monprojet-linux.yaml` qui doit se trouver dans le répertoire `pkg/env`. Cela va télécharger et installer tous les paquets dont vous avez besoin. A priori, vous n'aurez plus à y retoucher.

## 3. Mettre à jour un environnement conda

Il peut arrive qu'une nouvelle *release* des paquets propres au SED soit nécessaire pour continuer le projet. Dans ce cas, vous pouvez suivre le tuto ci-dessus, il suffit juste de changer `conda env create` par `conda env update` (en gardant le reste de la ligne.)

Si une mise à jour ne suffit pas et qu'il est nécessaire de re-créer intégralement l'environnement conda, vous pouvez le faire avec les commandes suivantes :

```
conda env list
conda env remove -n le_nom_de_mon_environnement
conda clean --all
conda clena --force-pkgs-dir
conda env create -f pkg/env/monprojet-linux.yaml
```

## 4. Quelques commandes utiles de conda

- `conda activate le_nom_de_mon_environnement` : vous permet d'utiliser les binaires stockés dans votre environnement (voir plus tard)
- `conda deactivate` : vous permet de désactiver un environnement précédemment activé
- `conda env list` : vous permet de vos vos environnement et savoir lequel est activé
- `conda list` : vous permet de savoir quels sont les paquets conda installés dans votre environnement.

Bon! On a notre projet, on a notre environnement conda associé, il ne reste plus qu'à paramétrer la compilation et compiler !

![](https://media1.tenor.com/images/28230f49f74b133f164aba4e0658715f/tenor.gif?itemid=19348685)
