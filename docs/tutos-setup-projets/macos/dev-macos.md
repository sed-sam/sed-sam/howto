[Retour à l'index](../README.md)

Vous avez besoin des outils de développement en ligne de commande de macOS pour compiler des projets C++. Vous devrez pouvoir les retrouver sur le site d'Apple : [developer.apple.com](https://developer.apple.com).

Demander un coup de main à un ingénieur du SED, puis demandez-lui d'écrire la doc (ou écrivez-la vous-même !)