Good luck, have fun.
![](https://media1.tenor.com/images/b1516b1b83ae49f6bbd727e7c66227bb/tenor.gif?itemid=19511421)

*Mais, mais... c'est dégueulasse !?*

## 1. Considérations générales

**conda-bundle** est tiré de **conda-constructor** et est censé gérer Windows, macOS et Linux. Malheureusement je n'ai pas de mac, et la priorité était de faire des releases Windows donc je n'ai poussé le concept que de ce côté là.

Pour Windows, conda-bundle utilise NSIS. Vous aurez sûrement besoin de Visual Studio Code avec l'extension NSIS pour avoir la coloration syntaxique. Puisque le template ne porte pas l'extension NSIS, pour activer la coloration syntaxique vous pouvez cliquer dans le coin inférieur droit qui indique le type de fichier, puis choisir "NSIS".



![](https://media1.tenor.com/images/5460d3145f17b21d3a87eb76340d8ddb/tenor.gif?itemid=15166606)

*Moi quand je dois hacker du python qui réécrit un template de script NSIS*
