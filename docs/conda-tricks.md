[Retour à l'index](./README.md)


This tricks come from the following reference:

* `Channels and generating an index <https://docs.conda.io/projects/conda-build/en/latest/concepts/generating-index.html>`__


How to add a local folder as custom channel
-------------------------------------------

For several reasons, it can be practical to download conda packages and to put them into a local folder that one would like to use as a local custom channel.

Let us imagine that one downloads package :code:`ffmpeg-4.2.2` for linux-64 and one wants to make it available through a local custom channel :code:`my_channel`.

Firstly, one has to create and organize the folder :code:`my_channel` as follows:

.. code-block:: bash

  $ mkdir my_channel
  $ cd my_channel
  $ mkdir linux-64
  $ mv $PATH_TO_PACKAGE/ffmpeg-4.2.2-h20bf706_0.tar.bz2 linux-64/.

  my_channel
  ├── linux-64
  │   └── ffmpeg-4.2.2-h20bf706_0.tar.bz2 
  ...

In general, a channel must fit with the following structure:

.. code-block:: bash

 my_channel
  ├── linux-32
  ├── linux-64
  ├── win-64
  ├── win-32
  ├── osx-64
  ...
  
Then one has to index the folder as follows:
 
.. code-block:: bash

  $ conda index .
  
This will generate both channeldata.json for the channel and repodata.json for the linux-64 subdir, along with some other files.

One can then check by doing

.. code-block:: bash

  $ conda search -c file:/<path to>/my_channel ffmpeg | grep my_channel
  

How to use custom local channel in a environment
------------------------------------------------

A custom local channel can be used into a :code:`yml` file as follows:

.. code-block:: yaml

  name: my_env
  channels:
     - file:///path_to/my_channel
  dependencies:
     - ffmpeg
    
