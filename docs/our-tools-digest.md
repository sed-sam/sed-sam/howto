[Retour à l'index](./README.md)


# our tools digest

Digest of our tools/processes

## git usage (15')

* git is a version control system

![](./images/git-version-control.png)

* global view of workflow

![](./images/git-global-view.png)

* review of main commands: [https://www.atlassian.com/dam/jcr:e7e22f25-bba2-4ef1-a197-53f46b6df4a5/SWTM-2088_Atlassian-Git-Cheatsheet.pdf]()

* branches/conflicts

![](./images/git-branches.png)

* [https://digitalvarys.com/git-branch-and-its-operations/]()
* [https://developerhowto.com/2018/10/12/git-for-beginners/]()


## gitlab (15')

* show the web interface

  * issues
  * milestones
  * labels
  * different views (list, dashboard)
  * roles (guest..owner)
  * merge request

* initializing a user's ssh public key, to allow actions using the git command

  * explain git+ssh
  * debug it (ssh git@gitlab.inria.fr)


## git flow (15')

* convention on:

  * branch names
  * version numbering

![](./images/git-flow.png)

* [https://danielkummer.github.io/git-flow-cheatsheet/]()

* show an example of gitflow & merge requests


# continous integration (15')

* big picture / demo on any gitlab project
* communication gitlab.inria.fr/ci.inria.fr
* gitlab-ci and wikipages: example of ct-gallery

  * [https://gitlab.inria.fr/ct/gallery/-/blob/master/.gitlab-ci.yml]()
  * [https://ct.gitlabpages.inria.fr/gallery/]()


## conda (15')

* big picture
* install miniconda.3: [https://docs.conda.io/en/latest/miniconda.html]()
* anaconda.org & channels
* play with env

```
conda env create -f ./env/sandox.yaml
conda activate sandbox
which python
```

# gui for debugging

Inria has licences for DDT debugger (can be used to debug mpi/openmpn programs, but not only. handle QString natively!). Download URL:
https://developer.arm.com/tools-and-software/server-and-hpc/downloads/arm-forge

Must be on the Inria network to have access to the license server, more information on this:
https://intranet.inria.fr/Vie-scientifique/Experimentation-Dev/ARM-ex-Allinea

[****see corresponding file in our private gitlab repo.**](https://gitlab.inria.fr/sed-sam/sed-sam/howto/-/tree/master/docs-private/our-tools-digest.md)

Alternatively you can use Visual Studio Code for easy visual debugging in C++, Python, and more!(tuto coming soon!)


# agile development (10')

* main principles: [https://fr.wikipedia.org/wiki/M%C3%A9thode_agile]()
* simplified sed implementation: [https://iww.inria.fr/sed-sophia/fr/retour-dexperience-methode-agile-simplifiee/]()

![](./images/Scrum-process-schema-EN-small.png)

* scrum: [https://fr.wikipedia.org/wiki/Scrum_(d%C3%A9veloppement)]()
* roles: scrum master, product owner
* standup meeting (yesterday, today, problem)
* user stories, tasks, use of gitlab issues, life cycle of an issue
* definition of "done"
* end of cycle demo

* merge process demo: (aka. gitflow/gitlab/ci/test/issues all together !!!)


# material / hands on

* configure your gitlab account
* put your ssh public key on gitlab.inria.fr
* **git clone** the project skeleton
* configure and start the project **conda** environment
* configure **git-flow** (git-flow init)
* enjoy the code sprint !!!
