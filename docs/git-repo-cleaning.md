# Git Repository Cleaning
`git rm` is not enough, since the purpose of a SCM is to keep the history of the repository. So the file removal needs a history rewriting. The current recommended command is git filter-repo, and the usage of `git filter-branch` or `bfg` is discouraged.
```bash
git clone git@gitlab.inria.fr:namespace/repository_name.git
cd repository_name

# removes path_to_remove (i.e. keeps everything except path_to_remove)
git filter-repo --path path_to_remove --invert-paths
git gc --aggressive --prune=now 

# unprotect the branches of the repository: on gitlab, go to the 
"repository/Settings/Repository/Protected branches"
git push --all --force git@gitlab.inria.fr:namespace/repository_name.git
git push --tags --force git@gitlab.inria.fr:namespace/repository_name.git
# restore the protection n the branches of the repository
```

Note that the repository on the gitlab server should not see its size be reduced immediately.
