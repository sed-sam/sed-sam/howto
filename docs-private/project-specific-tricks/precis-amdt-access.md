[Retour à l'index](./README.md)


# Access PRECIS amdt resources

## gitlab

* software : https://gitlab.inria.fr/coffee/precis
* test data : https://gitlab.inria.fr/coffee/precis-data
* plugins : https://gitlab.inria.fr/coffee/precis-plugins https://gitlab.inria.fr/coffee/precis-recipes
* documentation : https://coffee.gitlabpages.inria.fr/precis/

## com channels

* amdt-precis@inria.fr
* amdt-precis-dev@inria.fr
* server discord precis

## CI precis 

https://ci.inria.fr/project/precis-amdt
access granted for coffee and sed team members

## slave CI Fedora29

Configure your ssh client : https://ci.inria.fr/project/precis-amdt/slaves

ssh ci@precis-amdt-fedora-29.ci
* password : precis-slave

ssh root@precis-amdt-fedora29.ci
* password : precis-slave-admin

## CI token for gitlab

"token associated to the GitLab CI Service URL" ; can be read from jenkins > (slave name) > "ce qui declenche le build" > advanced > secret token :
* jenkins job secret token for precis-mac-10-13 slave : 811df9e8bdf223b84e300cca852f2fa8
* jenkins job secret token for precis-fedora-29 slave : 811df9e8bdf223b84e300cca852f2fa8

## gitlab account for CI

* gitlab username precis-gitlab
* gitlab password VUZDtnka2685{]++
* type: standard, not iLDAP

